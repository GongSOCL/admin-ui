var toolLibrary = {
  cookie: function (name){
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
      return unescape(arr[2]);
    } else {
      return null;
    }
  },
  getParam: function (key){
    var url = window.document.location.href.toString(); //当前完整url
    var u = url.split("?"); //以？为分隔符把url转换成字符串数组
    try {
      if (typeof(u[1]) == "string") {
        u = u[1].split("&"); //同上
        var get = {};
        for (var i in u) {
          var j = u[i].split("="); //同上
          get[j[0]] = j[1];
        }
        if(key){
          var val = get[key] ? get[key] : '';
          if(val) val = decodeURI(val);
          return val;
        }
        return get;
      } else {
        return {};
      }
    }catch (e) {
      return {};
    }
  },
  in_array: function (search, array) {
    for(var i in array){
      if(array[i] == search){
        return true;
      }
    }
    return false;
  },
  ajax: function (url, method, data, successCallback, errorCallback){
    var token = this.cookie('youyao_admin_token');
    var header = {
      Authorization: token
    };
    var request = $.ajax({
      url: url,
      method: method,
      timeout: 30000, // 超时时间设置，单位毫秒
      data: data,
      headers: header,
    });
    request.done(function(res) {
      layer.closeAll();
      return successCallback(res);
    });
    request.fail(function(jqXHR) {
      layer.closeAll();
      console.error(jqXHR);
      return errorCallback(jqXHR);
    });
  },
  viewRender: function (elem, callback, url, reqMethod = 'POST', data = ''){
    var view = $(elem).html();
    this.ajax(url, reqMethod, data, function(res){
      laytpl($(elem).html()).render(res.data, function(html){
        $(elem).html(html);
        if(callback != undefined){
          callback(res.data);
        }
      });
    }, function(xhr){
      alert('页面渲染失败: '+(xhr.responseJSON.msg ? xhr.responseJSON.msg : xhr.statusText));
    });
  }
};

window.arr_foreach = function(arr, fun){
  var html = '';
  try{
    for(i in arr){
      html += fun(i, arr[i]);
    }
  }catch (e) {
    //console.warn(e);
  }

  return html;
}
