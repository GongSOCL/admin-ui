import request from '@/utils/request'

// get upload token
export function getToken() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/upload/uploadToken',
    method: 'post'
  })
}

// 文章
export function articles() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/getArticles',
    method: 'get'
  })
}

// 资讯类型
export function informationType() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/information_type',
    method: 'get'
  })
}


// 小程序资源
export function res_list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/com_list',
    method: 'get'
  })
}

export function user_list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/msg_user_list',
    method: 'get'
  })
}

export function product_list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_list_com',
    method: 'get'
  })
}

// 问卷资源
export function q_resource_list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/q_resource_list',
    method: 'get'
  })
}
