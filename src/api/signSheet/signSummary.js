import request from '@/utils/request'

// 科室汇总
export function depart(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/depart',
    method: 'post',
    data
  })
}

// 大区汇总
export function area(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/area',
    method: 'post',
    data
  })
}

// 省份汇总
export function province(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/province',
    method: 'post',
    data
  })
}

// 医院汇总
export function hospital(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/hospital',
    method: 'post',
    data
  })
}

// 科室汇总导出
export function exportDepart(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/exportDepart',
    method: 'post',
    data
  })
}

// 大区汇总导出
export function exportArea(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/exportArea',
    method: 'post',
    data
  })
}

// 省份汇总导出
export function exportProvince(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/exportProvince',
    method: 'post',
    data
  })
}

// 医院汇总导出
export function exportHospital(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/sign/exportHospital',
    method: 'post',
    data
  })
}
