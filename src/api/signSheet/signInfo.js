import request from '@/utils/request'

// 列表
export function liveList() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/liveList',
    method: 'get'
  })
}

// 列表
export function seriesList() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/seriesList',
    method: 'get'
  })
}

// 直播列表
export function liveSignInfo(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/liveSignInfo',
    method: 'post',
    data
  })
}

// 直播导出
export function liveSignInfoExport(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/liveSignInfoExport',
    method: 'post',
    data
  })
}

// 大咖列表
export function seriesSignInfo(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/seriesSignInfo',
    method: 'post',
    data
  })
}

// 大咖导出
export function seriesSignInfoExport(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/seriesSignInfoExport',
    method: 'post',
    data
  })
}

// 服务商
export function searchDealer(name, limit) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/search_dealer',
    method: 'get',
    params: { 'search_dealer_name': name, 'limit': limit }
  })
}

// 数据落地
export function dataLanding() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/signinfoLanding',
    method: 'get'
  })
}
