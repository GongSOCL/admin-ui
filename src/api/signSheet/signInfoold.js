import request from '@/utils/request'

// 直播列表
export function liveSignInfo(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/oldsheet/liveSignInfo',
    method: 'post',
    data
  })
}

// 直播导出
export function liveSignInfoExport(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/oldsheet/liveSignInfoExport',
    method: 'post',
    data
  })
}
