import request from '@/utils/request'

//获取报名表
export function signSheet(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/signSheet',
    method: 'post',
    data
  })
}

//报名表详情
export function signSheetDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/signSheetDetail',
    method: 'post',
    data
  })
}

//新增报名表
export function createSignSheet(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/createSignSheet',
    method: 'post',
    data
  })
}

//删除报名表
export function deleteSignSheet(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/deleteSignSheet',
    method: 'post',
    data
  })
}

//获取所有报名表
export function allSignSheet(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/sheet/allSignSheet',
    method: 'post',
    data
  })
}
