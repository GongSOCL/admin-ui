import request from '@/utils/request'
// 搜索
export function search() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/list',
    method: 'get'
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/detail',
    method: 'post',
    data
  })
}

// 添加
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/update',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/delete',
    method: 'post',
    data
  })
}

// 添加
export function subUpdate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/subUpdate',
    method: 'post',
    data
  })
}

// 详情
export function subDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/subDetail',
    method: 'post',
    data
  })
}

// 删除
export function subDeleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/hot/subDelete',
    method: 'post',
    data
  })
}
