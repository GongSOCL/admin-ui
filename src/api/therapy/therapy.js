import request from '@/utils/request'

// 搜索
export function search() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/list',
    method: 'get'
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/detail',
    method: 'post',
    data
  })
}

// 添加
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/update',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/delete',
    method: 'post',
    data
  })
}

// 添加
export function subUpdate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/subUpdate',
    method: 'post',
    data
  })
}

// 详情
export function subDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/subDetail',
    method: 'post',
    data
  })
}

// 删除
export function subDeleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/subDelete',
    method: 'post',
    data
  })
}

// 产品
export function products() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/getProducts',
    method: 'get'
  })
}

// 文章
export function articles() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/info/therapy/getArticles',
    method: 'get'
  })
}
