import request from '@/utils/request'

export function getSmsCodes() {
    return request({
        url: '/smsapp/codes',
        method: 'get'
    })
}


// 优药app最近短信获取
export function getYouyaoAppSms() {
    return request({
        url: '/sms/youyao-app',
        method: 'get'
    })
}
