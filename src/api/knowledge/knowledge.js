import request from '@/utils/request'

// 查询列表
export function listKnowledge(query) {
  return request({
    url: '/knowledge/know_dir_list',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getKnowledgeDt(id) {
  return request({
    url: `/knowledge/know_dir_dtl`,
    method: 'get',
    params: { 'id': id }
  })
}

// 新增
export function addKnowledge(data) {
  return request({
    url: '/knowledge/know_dir_edit',
    method: 'post',
    data: data
  })
}

// 修改
export function updateKnowledge(data) {
  return request({
    url: `/knowledge/know_dir_edit`,
    method: 'post',
    data: data
  })
}

// 删除
export function delKnowledge(data) {
  return request({
    url: `/knowledge/know_dir_del`,
    method: 'post',
    data
  })
}


// 部门和下面的所有人 + 回显列表
export function getDepartmentsUsers() {
  return request({
    url: `/knowledge/power_list`,
    method: 'get'
  })
}


// 权限提交
export function PowerEdit(data) {
  return request({
    url: `/knowledge/power_edit`,
    method: 'post',
    data
  })
}


// 权限提交
export function getDepartmentsUsersDetail(id) {
  return request({
    url: `/knowledge/power_dtl`,
    method: 'get',
    params: { 'id': id }
  })
}
export const fileSize = 20
