import request from '@/utils/request'

export function getSmsCodes() {
    return request({
        url: '/sms/codes',
        method: 'get'
    })
}
