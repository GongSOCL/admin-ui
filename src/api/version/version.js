import request from '@/utils/request'

// getToken
export function getToken(type) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/upload/uploadToken',
    method: 'post',
    data: { 'type': type }
  })
}

// 版本列表
export function list(data) {
  return request({
    url: '/version/list',
    method: 'post',
    data
  })
}

// 版本信息
export function detail(data) {
  return request({
    url: '/version/detail',
    method: 'post',
    data
  })
}

// 添加版本
export function update(data) {
  return request({
    url: '/version/update',
    method: 'post',
    data
  })
}

// 删除版本
export function deleted(data) {
  return request({
    url: '/version/delete',
    method: 'post',
    data
  })
}
