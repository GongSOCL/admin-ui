import request from '@/utils/request'

// 渠道
export function channel() {
  return request({
    url: '/version/channel',
    method: 'get'
  })
}

// 版本列表
export function list(data) {
  return request({
    url: '/version/check/list',
    method: 'post',
    data
  })
}

// 版本信息
export function detail(data) {
  return request({
    url: '/version/check/detail',
    method: 'post',
    data
  })
}

// 添加版本
export function update(data) {
  return request({
    url: '/version/check/update',
    method: 'post',
    data
  })
}

// 通过审核
export function pass(data) {
  return request({
    url: '/version/check/pass',
    method: 'post',
    data
  })
}
