import request from '@/utils/request'

// 版本列表
export function list() {
  return request({
    url: '/version/otc/list',
    method: 'get'
  })
}

// 添加版本
export function update(data) {
  return request({
    url: '/version/otc/update',
    method: 'post',
    data
  })
}

// 删除版本
export function pass(data) {
  return request({
    url: '/version/otc/pass',
    method: 'post',
    data
  })
}
