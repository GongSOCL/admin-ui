import request from '@/utils/request'

export function search() {
  return request({
    url: '/label/search',
    method: 'get'
  })
}

export function detail(data) {
  return request({
    url: '/label/detail',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/label/update',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/label/delete',
    method: 'post',
    data
  })
}

export function deletedAll(data) {
  return request({
    url: '/label/deleteAll',
    method: 'post',
    data
  })
}

export function forSelect() {
  return request({
    url: '/label/forSelect',
    method: 'get'
  })
}

export function updateSort(data) {
  return request({
    url: '/label/updateSort',
    method: 'post',
    data
  })
}
