import request from '@/utils/request'

// 获取省份
export function getProvince(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/getProvinces',
    method: 'post',
    data
  })
}

// 获取分类
export function getType2(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/getType2',
    method: 'post',
    data
  })
}

// 列表
export function meetingPlanList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/meetingPlanList',
    method: 'get',
    params: data
  })
}

// 获取
export function meetingPlanDetail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/meetingPlanDetail',
    method: 'get',
    params: {
      id: id
    }
  })
}

// 新增/修改
export function meetingPlanEdit(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/meetingPlanEdit',
    method: 'post',
    data
  })
}

// 删除
export function meetingPlanDel(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/meetingPlanDel',
    method: 'post',
    data
  })
}
