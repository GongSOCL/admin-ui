import request from '@/utils/request'

// 获取直播类型
export function getType1() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/type1',
    method: 'get'
  })
}

// 获取直播类型
export function getType2(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/type2?type1_id='+id,
    method: 'get'
  })
}

// 获取直播类型
export function getType3(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/type3?type1_id='+id,
    method: 'get'
  })
}

// 获取直播类型
export function getTypes() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/types',
    method: 'get'
  })
}

// 获取用户
export function userByphone(phone) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/userByphone?phone='+phone,
    method: 'get'
  })
}

// 获取大区
export function area() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/area',
    method: 'get'
  })
}

// 获取省份
export function getProvince(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/province?area_id='+id,
    method: 'get'
  })
}

// 获取部门
export function getDepart(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/getDepart',
    method: 'get',
    data
  })
}

// 获取药品
export function product() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/product',
    method: 'get'
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/search',
    method: 'post',
    data
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/update',
    method: 'post',
    data
  })
}

export function end(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/end',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/delete',
    method: 'post',
    data
  })
}

// 置顶
export function top(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/top',
    method: 'post',
    data
  })
}

// 取消置顶
export function cancelTop(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/cancelTop',
    method: 'post',
    data
  })
}

// 上架
export function publish(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/publish',
    method: 'post',
    data
  })
}

// 下架
export function out(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/out',
    method: 'post',
    data
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/detail',
    method: 'post',
    data
  })
}

// 排序
export function rank(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/rank',
    method: 'post',
    data
  })
}

// 复制
export function copyCreate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/copyCreate',
    method: 'post',
    data
  })
}

// 统计数据
export function getAnalysisData(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/getAnalysis',
    method: 'post',
    data
  })
}

// 索引同步
export function updateEs(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/es',
    method: 'post',
    data
  })
}

// 优锐医院
export function getYouruiHospitals(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/getYouruiHospitals',
    method: 'post',
    data
  })
}

// 下载模版
export const downloadTmp = process.env.VUE_APP_BASE_API + '/live/downloadWhiteListTemplate'

// 上传白名单
export const whiteListExp = process.env.VUE_APP_BASE_API + '/live/whiteListExp'

export const fileSize = 20

// 会议列表
export function meetingList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/meeting/meetingList?name='+data,
    method: 'get'
  })
}

// 会议详情
export function meetingDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/meeting/meetingDetail',
    method: 'post',
    data
  })
}


//
export function getMeetingCategoryStatus(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/meeting_category_st?type1_id='+id,
    method: 'get',
  })
}


//  直播下拉选择列表
export function LiveSelect(live_id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/live_selected?live_id='+live_id,
    method: 'get',
  })
}


//  直播下拉选择列表
export function LiveList() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/live_select_list',
    method: 'get',
  })
}

//  生成二维码
export function createQRCode(id, type, is_online) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/qr_code?live_id='+id+'&type='+type+'&is_online='+is_online,
    method: 'get',
  })
}

export const province = [
  { id: 1, title: '北京市' },
  { id: 2, title: '天津市' },
  { id: 3, title: '上海市' },
  { id: 4, title: '重庆市' },
  { id: 5, title: '河北省' },
  { id: 6, title: '山西省' },
  { id: 7, title: '辽宁省' },
  { id: 8, title: '吉林省' },
  { id: 9, title: '黑龙江省' },
  { id: 10, title: '江苏省' },
  { id: 11, title: '浙江省' },
  { id: 12, title: '安徽省' },
  { id: 13, title: '福建省' },
  { id: 14, title: '江西省' },
  { id: 15, title: '山东省' },
  { id: 16, title: '河南省' },
  { id: 17, title: '湖北省' },
  { id: 18, title: '湖南省' },
  { id: 19, title: '广东省' },
  { id: 20, title: '海南省' },
  { id: 21, title: '四川省' },
  { id: 22, title: '贵州省' },
  { id: 23, title: '云南省' },
  { id: 24, title: '陕西省' },
  { id: 25, title: '甘肃省' },
  { id: 26, title: '青海省' },
  { id: 27, title: '内蒙古自治区' },
  { id: 28, title: '广西壮族自治区' },
  { id: 29, title: '西藏自治区' },
  { id: 30, title: '宁夏回族自治区' },
  { id: 31, title: '新疆维吾尔自治区' },
  { id: 32, title: '香港特别行政区' },
  { id: 33, title: '澳门特别行政区' },
  { id: 34, title: '台湾省' }
]
