import request from '@/utils/request'

// 单个汇总
export function sign(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/single',
    method: 'post',
    data
  })
}

// 科室汇总
export function depart(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/depart',
    method: 'post',
    data
  })
}

// 大区汇总
export function area(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/area',
    method: 'post',
    data
  })
}

// 省份汇总
export function province(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/province',
    method: 'post',
    data
  })
}

// 医院汇总
export function hospital(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/hospital',
    method: 'post',
    data
  })
}

// 单个汇总导出
export function exportSingle(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/exportSingle',
    method: 'post',
    data
  })
}

// 单个汇总导出
export function sExportSingle(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/sExportSingle',
    method: 'post',
    data
  })
}

// 科室汇总导出
export function exportDepart(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/exportDepart',
    method: 'post',
    data
  })
}

// 大区汇总导出
export function exportArea(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/exportArea',
    method: 'post',
    data
  })
}

// 省份汇总导出
export function exportProvince(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/exportProvince',
    method: 'post',
    data
  })
}

// 医院汇总导出
export function exportHospital(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/summary/live/exportHospital',
    method: 'post',
    data
  })
}

export const monthList = [
  { id: 1, title: '一月' },
  { id: 2, title: '二月' },
  { id: 3, title: '三月' },
  { id: 4, title: '四月' },
  { id: 5, title: '五月' },
  { id: 6, title: '六月' },
  { id: 7, title: '七月' },
  { id: 8, title: '八月' },
  { id: 9, title: '九月' },
  { id: 10, title: '十月' },
  { id: 11, title: '十一月' },
  { id: 12, title: '十二月' }
]

export const areaList = [
  { id: 1, title: '北一区' },
  { id: 2, title: '北二区' },
  { id: 3, title: '东一区' },
  { id: 4, title: '东二区' },
  { id: 5, title: '东三区' },
  { id: 6, title: '中区' },
  { id: 7, title: '南区' },
  { id: 8, title: '西区' }
]
