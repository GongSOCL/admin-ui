import request from '@/utils/request'

//问卷列表
export function questionnaire() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/interactive/questionnaire',
    method: 'get'
  })
}

//题目列表
export function question(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/interactive/question',
    method: 'post',
    data
  })
}

//互动详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/interactive/detail',
    method: 'post',
    data
  })
}

//取消置顶
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/live/interactive/update',
    method: 'post',
    data
  })
}
