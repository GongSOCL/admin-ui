import request from '@/utils/request'

// 搜索
export function search(tid, page, page_size) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/guideList',
    method: 'get',
    params: {
      page: page,
      page_size: page_size,
      tid: tid
    }
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/guideDetail',
    method: 'get',
    params: {
      id: id
    }
  })
}

// 添加
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/guideEdit',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/guideDel',
    method: 'post',
    data
  })
}

// 资讯分类
// 搜索
export function typeSearch(page, page_size) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/typeList',
    method: 'get',
    params: {
      page: page,
      page_size: page_size
    }
  })
}

// 详情
export function typeDetail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/typeDetail',
    method: 'get',
    params: {
      id: id
    }
  })
}

// 添加
export function typeUpdate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/typeEdit',
    method: 'post',
    data
  })
}

// 删除
export function typeDeleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/information_management/typeDel',
    method: 'post',
    data
  })
}
