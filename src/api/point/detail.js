import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/point/details',
    method: 'get',
    params: query
  })
}
