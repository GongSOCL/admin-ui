import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/point/trials',
    method: 'get',
    params: query
  })
}

export function fetchStatus() {
  return request({
    url: '/point/trial/status',
    method: 'get'
  })
}

export function backgroundCheck(id) {
  return request({
    url: `/point/background-check/${id}`,
    method: 'post'
  })
}

export function pass(id, data) {
  return request({
    url: `/point/trial/${id}`,
    method: 'patch',
    data: data
  })
}

// 多选自动审核 - 初审
export function AuditOrders(ids, tp, reason) {
  return request({
    url: `/point/trial/audit_orders`,
    method: 'post',
    data: {'ids': ids,'tp': tp,'reason': reason}
  })
}

// 多选自动审核 - 复审
export function ReviewAuditOrders(ids, tp, reason) {
  return request({
    url: `/point/trial/review_audit_orders`,
    method: 'post',
    data: {'ids': ids,'tp': tp,'reason': reason}
  })
}


export function GoodsList() {
  return request({
    url: `/point/goods-names`,
    method: 'get',
  })
}

// 设置黑名单
export function SetBlacklist(uid, tp, user_type) {
  return request({
    url: `/point/setblacklist`,
    method: 'post',
    data: {'uid': uid,'tp': tp,'user_type': user_type}
  })
}


// 制单
export function voucherPreparationExamine(uid, tp, reason, user_type) {
  return request({
    url: `/point/voucher_preparation/examine`,
    method: 'post',
    data: {'ids': uid, 'tp': tp, 'reason': reason, 'user_type': user_type}
  })
}

// 流程明细
export function ProcessDetailedList(query) {
  return request({
    url: '/point/process_detailed/list',
    method: 'get',
    params: query
  })
}

// 支付页
export function PaymentList(query) {
  return request({
    url: `/point/payment-list`,
    method: 'get',
    params: query
  })
}

// 支付页
export function Payment(batch_num) {
  return request({
    url: `/point/payment/${batch_num}`,
    method: 'post',
    // data: {'uid': uid,'tp': tp}
  })
}


// 导出
export const DownLoadFinances = process.env.VUE_APP_BASE_API + '/point/dload-fi'
