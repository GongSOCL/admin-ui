import request from '@/utils/request'

export function addUser(uid) {
  return request({
    url: `/point/blacklist/${uid}`,
    method: 'post'
  })
}

export function removeUser(uid) {
  return request({
    url: `/point/blacklist/${uid}`,
    method: 'delete'
  })
}
