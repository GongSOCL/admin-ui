import request from '@/utils/request'

//搜索
export function doctorPointSearch(data) {
  return request({
    url: '/point/doctor/search?'+data,
    method: 'get',
    //data
  })
}
// 获取省份数据
export function getStoreProvince() {
  return request({
    url: '/point/doctor/province',
    method: 'get',
    //data
  })
}
// 获取城市数据
export function getStoreCity(provinceID) {
  return request({
    url: '/point/doctor/city?province_id='+provinceID,
    method: 'get',
    //data
  })
}

export const doctorPointExport = process.env.VUE_APP_BASE_API + '/point/doctor/export'
