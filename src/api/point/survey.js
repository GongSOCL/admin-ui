import request from '@/utils/request'

// 列表 - 活动商品
export function getActivityGoodsList() {
  return request({
    url: '/point/survey/activity/goods/search',
    method: 'GET'
  })
}

// 知情书列表
export function geInformedLetterList() {
  return request({
    url: '/point/survey/informed/letter/search',
    method: 'GET'
  })
}

// ---------------------------------------------------------------- //

// 列表
export function listInformedLetter(query) {
  return request({
    url: '/point/survey/informed/letter',
    method: 'GET',
    params: query
  })
}

// 编辑/新增
export function editInformedLetter(data) {
  return request({
    url: '/point/survey/informed/edit',
    method: 'POST',
    data: data
  })
}

// 删除
export function delInformedLetter(id) {
  return request({
    url: '/point/survey/informed/del',
    method: 'POST'
  })
}

// 详情
export function infoInformedLetter(id) {
  return request({
    url: `/point/survey/informed/dtl/${id}`,
    method: 'GET'
  })
}

// -------------------------------- 问卷 -------------------------------- //

// 列表
export function questList(query) {
  return request({
    url: '/point/survey/quest',
    method: 'GET',
    params: query
  })
}

// 编辑/添加
export function editQuest(data) {
  return request({
    url: '/point/survey/quest/edit',
    method: 'POST',
    data: data
  })
}

// 删除
export function delQuest(id) {
  return request({
    url: `/point/survey/quest/del/${id}`,
    method: 'POST'
  })
}

// 详情
export function infoQuest(id) {
  return request({
    url: `/point/survey/quest/dtl/${id}`,
    method: 'GET'
  })
}

// 更新状态
export function changeSt(id, st) {
  return request({
    url: `/point/survey/quest/changest/${id}`,
    method: 'POST',
    data: { 'is_publish': st }
  })
}
export const fileSize = 1

// 导出
export const QuestionImp = process.env.VUE_APP_BASE_API + '/point/survey/import/quest'

export const toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'], // 加粗 斜体 下划线 删除线 -----['bold', 'italic', 'underline', 'strike']
  ['blockquote', 'code-block'], // 引用  代码块-----['blockquote', 'code-block']
  [{
    header: 1
  }, {
    header: 2
  }], // 1、2 级标题-----[{ header: 1 }, { header: 2 }]
  [{
    list: 'ordered'
  }, {
    list: 'bullet'
  }], // 有序、无序列表-----[{ list: 'ordered' }, { list: 'bullet' }]
  [{
    script: 'sub'
  }, {
    script: 'super'
  }], // 上标/下标-----[{ script: 'sub' }, { script: 'super' }]
  [{
    indent: '-1'
  }, {
    indent: '+1'
  }], // 缩进-----[{ indent: '-1' }, { indent: '+1' }]
  [{
    'direction': 'rtl'
  }], // 文本方向-----[{'direction': 'rtl'}]
  [{
    size: ['12px', '14px', '16px', '20px', '24px', '36px']
  }], // 字体大小-----[{ size: ['small', false, 'large', 'huge'] }]
  [{
    header: [1, 2, 3, 4, 5, 6, false]
  }], // 标题-----[{ header: [1, 2, 3, 4, 5, 6, false] }]
  [{
    color: []
  }, {
    background: []
  }], // 字体颜色、字体背景颜色-----[{ color: [] }, { background: [] }]
  [{
    font: []
  }], // 字体种类-----[{ font: [] }]
  [{
    align: []
  }], // 对齐方式-----[{ align: [] }]
  ['clean'], // 清除文本格式-----['clean']
  ['link', 'image', 'video'] // 链接、图片、视频-----['link', 'image', 'video']
]

export function batchSendTopicPointRewards(id, upload_id) {
  return request({
    url: `/phy/topics/${id}/send-top-points`,
    method: 'POST',
    data: {
      upload_id
    }
  })
}
