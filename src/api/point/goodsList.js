import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/point/goods/list',
    method: 'get',
    params: query
  })
}


export function getDetail(id) {
  return request({
    url: '/point/goods/getDetail',
    method: 'get',
      params: { 'id': id,'status':999 }
  })
}

export function edit(data) {
  return request({
    url: '/point/goods/edit',
    method: 'post',
    data
  })
}

export function getToken() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/upload/uploadToken',
    method: 'post'
  })
}
