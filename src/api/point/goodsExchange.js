import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/point/goods_exc/list',
    method: 'get',
    params: query
  })
}


export function getDetail(id) {
  return request({
    url: '/point/goods_exc/getDetail',
    method: 'get',
    params: { 'id': id,'status':999 }
  })
}

export function fetchStatus() {
  return request({
    url: '/point/goods_exc/status',
    method: 'get'
  })
}


export function pass(id, status,trial_reject_reason,goods_name) {
  return request({
    url: '/point/goods_exc/examine',
    method: 'post',
    data: { 'id': id,'status':status,'trial_reject_reason':trial_reject_reason,'goods_name':goods_name }
  })
}

export function pointDetailed(data) {
  return request({
    url: '/point/goods_exc/point_detailed',
    method: 'get',
    params:data
  })
}
