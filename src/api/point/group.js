import request from '@/utils/request'

// 获取默认分组分组
export function getDefaultGroups(platform) {
    return request({
        url: '/point/groups/default',
        method: 'GET',
        params: {
            platform
        }
    })
}

// 搜索分组
export function searchGroups(platform, keywords = "", limit = 5) {
    return request({
        url: '/point/groups/search',
        method: 'GET',
        params: {
            platform,
            keywords,
            limit
        }
    })
}

// 根据分组id获取分组信息
export function infoGroup(id) {
    return request({
        url: `/point/groups/${id}`,
        method: 'GET'
    })
}