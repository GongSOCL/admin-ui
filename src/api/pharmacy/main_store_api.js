import request from '@/utils/request'

// 连锁店
export function get_main_store_list(data) {
  return request({
    url: '/pharmacy/store/main-store-list',
    method: 'GET',
    params: data
  })
}

// 门店
export function get_store_list(data) {
  return request({
    url: '/pharmacy/store/store-list',
    method: 'GET',
    params: data
  })
}
