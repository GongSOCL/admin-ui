import request from '@/utils/request'

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/sale/report/list',
    method: 'get',
    params: data
  })
}
export const sale_report_export_req = process.env.VUE_APP_BASE_API + '/pharmacy/sale/report/export'
