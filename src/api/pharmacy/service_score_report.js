import request from '@/utils/request'

// 搜索
export function main_store_service_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/service-score/report/main-store-list',
    method: 'get',
    params: data
  })
}

// 搜索
export function store_service_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/service-score/report/store-list',
    method: 'get',
    params: data
  })
}

// 搜索
export function clerk_service_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/service-score/report/clerk-list',
    method: 'get',
    params: data
  })
}

// 导出
export const main_store_service_score_report_export = process.env.VUE_APP_BASE_API + '/pharmacy/service-score/report/main-store-export'
export const store_service_score_report_export = process.env.VUE_APP_BASE_API + '/pharmacy/service-score/report/store-export'
export const clerk_service_score_report_export = process.env.VUE_APP_BASE_API + '/pharmacy/service-score/report/clerk-export'

