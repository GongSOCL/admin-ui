import request from '@/utils/request'

export function listMainStoresLimite(city_id = 0, keywords = '', limit = 5) {
  return request({
    url: '/pharmacy/store/search-main-store',
    method: 'GET',
    params: { city_id, keywords, limit }
  })
}

export function listBranchStoresLimit(
  area_id = 0,
  province_id = 0,
  city_id = 0,
  main_store_id = 0,
  keywords = '',
  limit = 5
) {
  return request({
    url: '/pharmacy/store/search-branch-store',
    method: 'GET',
    params: {
      area_id,
      province_id,
      city_id,
      main_store_id,
      keywords,
      limit
    }
  })
}

// 药店列表
export function getList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/store/list',
    method: 'post',
    data
  })
}

// 药店导出
export function exportList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/store/list-export',
    method: 'post',
    data
  })
}
