import request from '@/utils/request'

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/clerk/apply/list',
    method: 'post',
    data
  })
}

// 同意
export function agree(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/clerk/apply/agree',
    method: 'post',
    data
  })
}

// 拒绝
export function refuse(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/clerk/apply/refuse',
    method: 'post',
    data
  })
}

// 拒绝
export function cancel(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/clerk/apply/cancel',
    method: 'post',
    data
  })
}
