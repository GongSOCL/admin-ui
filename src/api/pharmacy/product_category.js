import request from '@/utils/request'

export function search_product_cat(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product-category-rel/list',
    method: 'get',
    params: data
  })
}
export function detail_product_cat(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product-category-rel/detail',
    method: 'get',
    params: data
  })
}

export function del_product_cat(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product-category-rel/delete',
    method: 'get',
    params: data
  })
}

export function save_product_cat(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product-category-rel/save',
    method: 'post',
    data: data
  })
}

export function product_cat_list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product-category-rel/cat-list',
    method: 'get',
    params: data
  })
}
