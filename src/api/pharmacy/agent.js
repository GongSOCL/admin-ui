import request from '@/utils/request'

export function exportPatientTicket(upload_id) {
    return request({
        url: '/pharmacy/agent/export-patient-ticket',
        method: "post",
        data: {
            upload_id
        }
    })
}

// 列出代表培训陈列数据
export function listTopicTrainingRecords(
  area_id = 0,
  province_id = 0,
  city_id = 0,
  main_store_id = 0,
  branch_store_id = 0,
  start = '',
  end = '',
  current = 1,
  limit = 10
) {
  return request({
    url: '/pharmacy/agent/record/topic-training',
    method: 'GET',
    params: {
      area_id,
      province_id,
      city_id,
      main_store_id,
      branch_store_id,
      start,
      end,
      current,
      limit
    }
  })
}

// 导出代表培训陈列数据
export function exportTopicTrainingRecords(
  area_id = 0,
  province_id = 0,
  city_id = 0,
  main_store_id = 0,
  branch_store_id = 0,
  start = '',
  end = ''
) {
  return request({
    url: '/pharmacy/agent/record/topic-training-export',
    method: 'POST',
    params: {
      area_id,
      province_id,
      city_id,
      main_store_id,
      branch_store_id,
      start,
      end
    }
  })
}
