import request from '@/utils/request'

export function mold_list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/mold_list',
    method: 'get'
  })
}

export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/res_list',
    method: 'get',
    params: data
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/res_list',
    method: 'post',
    data
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/res_edit',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/res_del',
    method: 'post',
    data
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/res_dtl',
    method: 'get',
    params: { 'id': id }
  })
}


export function getToken(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/upload/uploadToken',
    method: 'post',
    data
  })
}

// 评论
export function res_comment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/res_comment',
    method: 'get',
    params: data
  })
}

// 置顶
export function topComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/comment_top',
    method: 'post',
    data
  })
}

// 置顶
export function deleteComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/phy/comment_del',
    method: 'post',
    data
  })
}

export const fileSize = 20
