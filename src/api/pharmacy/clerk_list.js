import request from '@/utils/request'

// 搜索
export function clerk_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/clerk/list',
    method: 'get',
    params: data
  })
}

export const clerk_list_export = process.env.VUE_APP_BASE_API + '/pharmacy/clerk/export'
