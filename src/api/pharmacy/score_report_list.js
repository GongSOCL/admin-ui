import request from '@/utils/request'

// 搜索
export function main_store_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/score/report/main-store-list',
    method: 'get',
    params: data
  })
}

// 搜索
export function store_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/score/report/store-list',
    method: 'get',
    params: data
  })
}

// 搜索
export function clerk_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/score/report/clerk-list',
    method: 'get',
    params: data
  })
}

// 搜索
export function clerk_month_score_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/score/report/clerk-month-score-list',
    method: 'get',
    params: data
  })
}
// 导出
export const score_report_main_store_export = process.env.VUE_APP_BASE_API + '/pharmacy/score/report/main-store-export'
export const score_report_store_export = process.env.VUE_APP_BASE_API + '/pharmacy/score/report/store-export'
export const score_report_clerk_export = process.env.VUE_APP_BASE_API + '/pharmacy/score/report/clerk-export'
export const month_score_report_clerk_export = process.env.VUE_APP_BASE_API + '/pharmacy/score/report/clerk-month-score-export'
