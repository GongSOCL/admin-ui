import request from '@/utils/request'

export function listAreas(keywords = '') {
  return request({
    url: '/pharmacy/common/area',
    method: 'GET',
    params: { keywords }
  })
}

export function listProvinces(area_id = 0, keywords = '', limit = 5) {
  return request({
    url: '/pharmacy/common/area/provinces',
    method: 'GET',
    params: { keywords, area_id, limit }
  })
}

export function listCities(province_id = 0, keywords = '', limit = 5) {
  return request({
    url: '/pharmacy/common/area/cities',
    method: 'GET',
    params: { keywords, province_id, limit }
  })
}
