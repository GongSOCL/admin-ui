import request from '@/utils/request'

// 列表 - 资源
export function GetResList() {
  return request({
    url: '/phy/rs-list',
    method: 'GET'
  })
}

// 列表 - 活动 暂无...
export function GetActList() {
  return request({
    url: '/phy/act-list',
    method: 'GET'
  })
}

// 列表 - 问卷
export function GetQuestList() {
  return request({
    url: '/phy/qust-list',
    method: 'GET'
  })
}

// -------------------------- 话题 -------------------------- //
// 列表话题
export function listTopic(query) {
  return request({
    url: '/phy/topics/list',
    method: 'GET',
    params: query
  })
}

// 添加话题
export function addTopic(data) {
  return request({
    url: '/phy/topics/edit',
    method: 'POST',
    data: data
  })
}

// 编辑话题
export function editTopic(data) {
  return request({
    url: `/phy/topics/detail/${id}`,
    method: 'PUT'
  })
}

// 删除话题
export function delTopic(id) {
  return request({
    url: `/phy/topics/del/${id}`,
    method: 'POST'
  })
}

// 修改状态
export function status(id, status) {
  return request({
    url: `/phy/topics/publish/${id}`,
    method: 'PUT',
    data: {
      status
    }
  })
}

// 话题详情
export function infoTopic(id) {
  return request({
    url: `/phy/topics/detail/${id}`,
    method: 'GET'
  })
}

// -------------------------- 小知识 -------------------------- //

export function listHealth(query) {
  return request({
    url: '/phy/health_tips/list',
    method: 'GET',
    params: query
  })
}

export function addHealth(data) {
  return request({
    url: '/phy/health_tips/edit',
    method: 'POST',
    data: data
  })
}

export function infoHealth(id) {
  return request({
    url: `/phy/health_tips/detail/${id}`,
    method: 'GET'
  })
}

export function delHealth(id) {
  return request({
    url: `/phy/health_tips/del/${id}`,
    method: 'POST'
  })
}

// -------------------------- 反馈 -------------------------- //

export function listAuestionAnswer(query) {
  return request({
    url: '/phy/question_answer/list',
    method: 'GET',
    params: query
  })
}

export function addAuestionAnswer(data) {
  return request({
    url: '/phy/question_answer/edit',
    method: 'POST',
    data: data
  })
}

export function infoAuestionAnswer(id) {
  return request({
    url: `/phy/question_answer/detail/${id}`,
    method: 'GET'
  })
}

export function delAuestionAnswer(id) {
  return request({
    url: `/phy/question_answer/del/${id}`,
    method: 'POST'
  })
}

// -------------------------- 适应症 -------------------------- //

export function listIndication(query) {
  return request({
    url: '/phy/indication/list',
    method: 'GET',
    params: query
  })
}

export function addIndication(data) {
  return request({
    url: '/phy/indication/edit',
    method: 'POST',
    data: data
  })
}

export function infoIndication(id) {
  return request({
    url: `/phy/indication/detail/${id}`,
    method: 'GET'
  })
}

export function delIndication(id) {
  return request({
    url: `/phy/indication/del/${id}`,
    method: 'POST'
  })
}

// -------------------------- 指南 -------------------------- //

export function listGuide(query) {
  return request({
    url: '/phy/guide/list',
    method: 'GET',
    params: query
  })
}

export function addGuide(data) {
  return request({
    url: '/phy/guide/edit',
    method: 'POST',
    data: data
  })
}

export function infoGuide(id) {
  return request({
    url: `/phy/guide/detail/${id}`,
    method: 'GET'
  })
}

export function delGuide(id) {
  return request({
    url: `/phy/guide/del/${id}`,
    method: 'POST'
  })
}

// -------------------------- 问卷 -------------------------- //

export function listQuest(query) {
  return request({
    url: '/phy/quest/list',
    method: 'GET',
    params: query
  })
}

export function addQuest(data) {
  return request({
    url: '/phy/quest/edit',
    method: 'POST',
    data: data
  })
}

export function infoQuest(id) {
  return request({
    url: `/phy/quest/detail/${id}`,
    method: 'GET'
  })
}

export function delQuest(id) {
  return request({
    url: `/phy/quest/del/${id}`,
    method: 'POST'
  })
}

export function QuestSt(id, st) {
  return request({
    url: `/phy/quest/st/${id}`,
    method: 'GET',
    params: { 'status': st }
  })
}

// -------------------------- 参与记录 -------------------------- //

export function listRecord(query) {
  return request({
    url: '/phy/record/list',
    method: 'GET',
    params: query
  })
}

export function delRecord(id) {
  return request({
    url: `/phy/record/del/${id}`,
    method: 'DELETE'
  })
}

export function auditRecord(id, reason) {
  return request({
    url: `/phy/record/auditRecord/${id}`,
    method: 'PUT',
    data: { 'reason': reason }
  })
}

export function getProvinceList(is_regional_manager) {
  return request({
    url: `/phy/province/list`,
    method: 'get',
    params: { 'is_regional_manager': is_regional_manager }
  })
}

export function getCityList(data) {
  return request({
    url: `/phy/city/list`,
    method: 'get',
    params: data
  })
}

export function getMainStoreList(data) {
  return request({
    url: `/phy/mstore/list`,
    method: 'get',
    params: data
  })
}

export function getBranchStoreList(data) {
  return request({
    url: `/phy/store/list`,
    method: 'get',
    params: data
  })
}

// 用户搜索
export function limitSearch(keywords = '', limit = 10) {
  return request({
    url: '/phy/record/u-search',
    method: 'GET',
    params: {
      keywords,
      limit
    }
  })
}

// 搜索话题
export function searchTopic(keywords = '', limit = 10) {
  return request({
    url: '/phy/topics/search',
    method: 'GET',
    params: {
      keywords,
      limit
    }
  })
}

export function auditGoods(listQuery) {
  return request({
    url: '/phy/audit/goods',
    method: 'GET',
    params: listQuery
  })
}

// 搜索商品
export function goodsSearch(listQuery) {
  return request({
    url: '/phy/audit/goods-search',
    method: 'GET',
    params: listQuery
  })
}

// -------------------------- 考试 -------------------------- //

export function ExamList(query) {
  return request({
    url: '/phy/exam-list',
    method: 'GET',
    params: query
  })
}

export function ProList(query) {
  return request({
    url: '/phy/pro-list',
    method: 'GET',
    params: query
  })
}

export function MainList(query) {
  return request({
    url: '/phy/main-list',
    method: 'GET',
    params: query
  })
}

export function BranchStore(query) {
  return request({
    url: '/phy/branch-store',
    method: 'GET',
    params: query
  })
}

export function PharmacyExamList(query) {
  return request({
    url: '/phy/exam',
    method: 'GET',
    params: query
  })
}

export function PharmacyExamEdit(query) {
  return request({
    url: '/phy/exam/edit',
    method: 'POST',
    data: query
  })
}

export function PharmacyExamDel(id) {
  return request({
    url: `/phy/exam/del/${id}`,
    method: 'POST'
  })
}

export function PharmacyExamDetail(id) {
  return request({
    url: `/phy/exam/detail/${id}`,
    method: 'GET'
  })
}

// -------------------------- 动态资讯2 -------------------------- //

export function listDynamic(query) {
  return request({
    url: '/dynamic/information/list',
    method: 'GET',
    params: query
  })
}

export function addDynamic(data) {
  return request({
    url: '/dynamic/information/edit',
    method: 'POST',
    data: data
  })
}

export function infoDynamic(id) {
  return request({
    url: `/dynamic/information/detail/${id}`,
    method: 'GET'
  })
}

export function delDynamic(id) {
  return request({
    url: `/dynamic/information/del/${id}`,
    method: 'POST'
  })
}

// 导出
export const RecordExport = process.env.VUE_APP_BASE_API + '/phy/record/exp'
export const QuestExport = process.env.VUE_APP_BASE_API + '/phy/quest/exp'
export const GoodsExport = process.env.VUE_APP_BASE_API + '/phy/audit/goods/exp'
export const GoodsImp = process.env.VUE_APP_BASE_API + '/phy/audit/goods/imp'

export const toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'], // 加粗 斜体 下划线 删除线 -----['bold', 'italic', 'underline', 'strike']
  ['blockquote', 'code-block'], // 引用  代码块-----['blockquote', 'code-block']
  [{
    header: 1
  }, {
    header: 2
  }], // 1、2 级标题-----[{ header: 1 }, { header: 2 }]
  [{
    list: 'ordered'
  }, {
    list: 'bullet'
  }], // 有序、无序列表-----[{ list: 'ordered' }, { list: 'bullet' }]
  [{
    script: 'sub'
  }, {
    script: 'super'
  }], // 上标/下标-----[{ script: 'sub' }, { script: 'super' }]
  [{
    indent: '-1'
  }, {
    indent: '+1'
  }], // 缩进-----[{ indent: '-1' }, { indent: '+1' }]
  [{
    'direction': 'rtl'
  }], // 文本方向-----[{'direction': 'rtl'}]
  [{
    size: ['12px', '14px', '16px', '20px', '24px', '36px']
  }], // 字体大小-----[{ size: ['small', false, 'large', 'huge'] }]
  [{
    header: [1, 2, 3, 4, 5, 6, false]
  }], // 标题-----[{ header: [1, 2, 3, 4, 5, 6, false] }]
  [{
    color: []
  }, {
    background: []
  }], // 字体颜色、字体背景颜色-----[{ color: [] }, { background: [] }]
  [{
    font: []
  }], // 字体种类-----[{ font: [] }]
  [{
    align: []
  }], // 对齐方式-----[{ align: [] }]
  ['clean'], // 清除文本格式-----['clean']
  ['link', 'image', 'video'] // 链接、图片、视频-----['link', 'image', 'video']
]

export function batchSendTopicPointRewards(id, upload_id) {
  return request({
    url: `/phy/topics/${id}/send-top-points`,
    method: 'POST',
    data: {
      upload_id
    }
  })
}

export function rejectExchange(exchange_id, reason) {
  return request({
    url: '/phy/audit/exchange/revoke',
    method: 'PUT',
    data: { exchange_id, reason }
  })
}
