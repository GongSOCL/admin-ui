import request from '@/utils/request'

// 搜索
export function quest_report_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/quest-report/list',
    method: 'get',
    params: data
  })
}

export const quest_report_list_export = process.env.VUE_APP_BASE_API + '/pharmacy/quest-report/export'
