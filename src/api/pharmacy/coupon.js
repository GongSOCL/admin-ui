import request from '@/utils/request'

export function couponList(data) {
  return request({
    url: '/pharmacy/coupon/list',
    method: 'GET',
    params: data
  })
}

export function addCoupon(data) {
  return request({
    url: '/pharmacy/coupon/add-coupon',
    method: 'POST',
    data: data
  })
}

export function couponDetail(coupon_id) {
  return request({
    url: '/pharmacy/coupon/coupon-detail',
    method: 'GET',
    params: { coupon_id: coupon_id }
  })
}

export function delCoupon(coupon_id) {
  return request({
    url: '/pharmacy/coupon/del-coupon',
    method: 'GET',
    params: { coupon_id: coupon_id }
  })
}
export function upCoupon(coupon_id) {
  return request({
    url: '/pharmacy/coupon/up-coupon',
    method: 'GET',
    params: { coupon_id: coupon_id }
  })
}

export function offCoupon(coupon_id) {
  return request({
    url: '/pharmacy/coupon/off-coupon',
    method: 'GET',
    params: { coupon_id: coupon_id }
  })
}

