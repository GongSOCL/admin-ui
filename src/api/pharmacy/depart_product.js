import request from '@/utils/request'

// 列表科室-产品
export function listDepartProducts(depart_id = 0, product_id = 0, current = 1, limit = 10) {
  return request({
    url: '/pharmacy/depart-product',
    method: 'GET',
    params: { depart_id, product_id, current, limit }
  })
}

// 添加科室-产品
export function addDepartProduct(depart_id, product_id) {
  return request({
    url: '/pharmacy/depart-product',
    method: 'POST',
    data: { depart_id, product_id }
  })
}

// 编辑科室-产品
export function editDepartProduct(id, depart_id, product_id) {
  return request({
    url: `/pharmacy/depart-product/${id}`,
    method: 'PUT',
    data: { depart_id, product_id }
  })
}

// 删除科室-产品
export function delDepartProduct(id) {
  return request({
    url: `/pharmacy/depart-product/${id}`,
    method: 'DELETE'
  })
}
