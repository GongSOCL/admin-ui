import request from '@/utils/request'

// 搜索
export function topic_report_search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/topic-report/list',
    method: 'get',
    params: data
  })
}

export const topic_report_list_export = process.env.VUE_APP_BASE_API + '/pharmacy/topic-report/export'
