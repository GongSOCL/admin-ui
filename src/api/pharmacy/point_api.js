import request from '@/utils/request'

// 搜索
export function get_point_acts(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/point-act/list',
    method: 'get',
    params: data
  })
}
