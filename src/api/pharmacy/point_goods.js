import request from '@/utils/request'

export function listGoods(keywords= "", current = 1, limit = 10) {
    return request({
        url: "/pharmacy/point/goods",
        method: "GET",
        params: {
            keywords,
            current,
            limit
        }
    })
}

export function addGoods(
    name,
    start,
    cover_id,
    role,
    type = 1,
    status = 1,
    desc = "",
    money_point = 0.00,
    service_point = 0.00,
    total = 0,
    end = "",
    money = 0.00,
    external_goods_id = 0,
    group_id = 0
) {
    return request({
        url: "/pharmacy/point/goods",
        method: "POST",
        data: {
            name,
            start,
            cover_id,
            role,
            type,
            status,
            desc,
            money_point,
            service_point,
            total,
            end,
            money,
            external_goods_id,
            group_id
        }
    })
}

export function editGoods(
    id,
    name,
    start,
    cover_id,
    role,
    status = 1,
    desc = "",
    money_point = 0.00,
    service_point = 0.00,
    total = 0,
    end = "",
    money = 0.00,
    external_goods_id = 0,
    group_id = 0
) {
    return request({
        url: `/pharmacy/point/goods/${id}`,
        method: "PUT",
        data: {
            name,
            start,
            cover_id,
            role,
            status,
            desc,
            money_point,
            service_point,
            total,
            end,
            money,
            external_goods_id,
            group_id
        }
    })
}

export function delGoods(id) {
    return request({
        url: `/pharmacy/point/goods/${id}`,
        method: "DELETE"
    })
}


export function enableGoods(id) {
    return request({
        url: `/pharmacy/point/goods/${id}/enable`,
        method: "PUT"
    })
}


export function disableGoods(id) {
    return request({
        url: `/pharmacy/point/goods/${id}/disable`,
        method: "PUT"
    })
}

export function infoGoods(id) {
    return request({
        url: `/pharmacy/point/goods/${id}`,
        method: "GET"
    })
}

export function searchValidCoupon(keywords = "", limit = 5) {
    return request({
        url: "pharmacy/point/goods/coupons",
        method: "GET",
        params: {keywords, limit}
    })
}