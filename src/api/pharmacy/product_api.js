import request from '@/utils/request'

// 搜索
export function get_products(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product/product-list',
    method: 'get',
    params: data
  })
}

// 搜索
export function get_skus(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/pharmacy/product/sku-list',
    method: 'get',
    params: data
  })
}

