import request from '@/utils/request'

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/video/search',
    method: 'post',
    data
  })
}

// 添加
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/video/update',
    method: 'post',
    data
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/video/detail',
    method: 'post',
    data
  })
}

// 删除评论
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/video/delete',
    method: 'post',
    data
  })
}

export function allVideos() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/video/allVideos',
    method: 'get'
  })
}

export function searchById(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/video/searchById',
    method: 'post',
    data
  })
}
