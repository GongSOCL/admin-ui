import request from '@/utils/request'

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/product/search',
    method: 'post',
    data
  })
}

// 添加
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/product/update',
    method: 'post',
    data
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/product/detail',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/resources/product/delete',
    method: 'post',
    data
  })
}
