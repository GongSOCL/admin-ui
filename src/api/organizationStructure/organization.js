import request from '@/utils/request'

// 树结构
export function dingtalkTreeList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/organization/dingtalk/department/tree',
    method: 'get',
    params: data
  })
}
// 人员列表
export function dingtalkUsers(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/organization/dingtalk/users',
    method: 'get',
    params: data
  })
}

//   组织架构树列表
export function depTree(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/organization/departments/tree',
    method: 'get',
    params: data
  })
}

//   一键同步
export function syncDingtalk(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/organization/dingtalk/sync',
    method: 'post',
    data
  })
}
//   从Excel导入
export function excelImport(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/organization/excel/sync',
    method: 'post',
    data
  })
}
//   添加部门
export function addDep(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/organization/departments',
    method: 'post',
    data
  })
}
//   重命名部门
export function renameDep(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${id}/rename`,
    method: 'put',
    data
  })
}
//   删除部门
export function deleteDep(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${data.id}`,
    method: 'delete',
    data
  })
}
//   移动部门
export function moveOrder(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${id}/order`,
    method: 'put',
    data
  })
}
//   绑定部门
export function bindDep(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${id}/bind-department`,
    method: 'put',
    data
  })
}
//   绑定对应关系显示
export function bindDepList(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${id}/bind-departments`,
    method: 'get'
  })
}
// 部门人员列表
export function depUsers(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${data.id}/users`,
    method: 'get'
  })
}
// 设置管理员
export function setUser(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/department-user/${id}/manager`,
    method: 'put'
  })
}
// 手动绑定人员
export function bindUser(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/departments/${id}/bind-user`,
    method: 'put',
    data
  })
}
// 手动绑定人员详情
export function orUserDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/department-user/${data.id}`,
    method: 'get'
    // params:data
  })
}
// 修改手机号
export function mobile(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/department-user/${id}/mobile`,
    method: 'put',
    data
  })
}
// 修改邮箱
export function email(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/department-user/${id}/mail`,
    method: 'put',
    data
  })
}
// 操作离职
export function quitJob(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/department-user/${id}`,
    method: 'delete'
  })
}
// 获取角色列表或者新增角色
export function roles(type, data) {
  if (type == 'list') {
    return request({
      baseURL: process.env.VUE_APP_BASE_API,
      url: `/organization/roles`,
      method: 'get',
      params: data
    })
  } else if (type == 'add') {
    return request({
      baseURL: process.env.VUE_APP_BASE_API,
      url: `/organization/roles`,
      method: 'post',
      data
    })
  }
}
// 编辑角色
export function updateRole(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/roles/${id}`,
    method: 'put',
    data
  })
}
// 删除角色
export function delRole(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/roles/${id}`,
    method: 'delete'
  })
}
// 更新状态
export function updateStatus(id, data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: `/organization/roles/${id}/status`,
    method: 'put',
    data
  })
}

export function getYyAppMenu(id) {
  return request({
    url: `/organization/roles/yyappmenu?id=${id}`,
    method: 'get'
  })
}

// 设置角色
export function setRole(data) {
  return request({
    url: `/organization/departments/userrole`,
    method: 'post',
    data: data
  })
}

/**
 * 获取角色组列表
 * @param data
 * @returns {AxiosPromise}
 */
window.roleGroupList = function(data) {
  return request({
    url: 'organization/rolesGroup?page=' + data.page + '&limit=' + data.limit + '&dep_id=' + data.dep_id,
    method: 'GET',
    data: {}
  })
}

/**
 * 部门绑定角色
 * @param data
 * @returns {AxiosPromise}
 */
window.bindRoleGroup = function(id, groupIds) {
  return request({
    url: '/organization/departments/bind-role-group',
    method: 'POST',
    data: {'id': id, 'role_groups': groupIds }
  })
}
