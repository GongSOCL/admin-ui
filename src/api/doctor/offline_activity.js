import request from '@/utils/request'

// 线下活动列表
export function listOfflineActivities(company_id, start, end, keywords = '', current = 1, limit = 10) {
  return request({
    url: '/doctor/offline-activity',
    method: 'GET',
    params: {
      company_id,
      start,
      end,
      keywords,
      current,
      limit
    }
  })
}

// 添加线下活动
export function addOfflineActivity(company_id, name, start, end, no = '') {
  return request({
    url: '/doctor/offline-activity',
    method: 'POST',
    data: {
      company_id,
      name,
      start,
      end,
      no
    }
  })
}

// 编辑线下活动
export function editOfflineActivity(id, company_id, name, start, end, no = '') {
  return request({
    url: `/doctor/offline-activity/${id}`,
    method: 'PUT',
    data: {
      company_id,
      name,
      start,
      end,
      no
    }
  })
}

// 线下活动详情
export function infoOfflineActivity(id) {
  return request({
    url: `/doctor/offline-activity/${id}`,
    method: 'GET'
  })
}

// 删除线下活动
export function delOfflineActivity(id) {
  return request({
    url: `/doctor/offline-activity/${id}`,
    method: 'DELETE'
  })
}

// 配置报名表
export function assignOfflineActivitySheet(id, sheet_id) {
  return request({
    url: `/doctor/offline-activity/${id}/assign-sheet`,
    method: 'PUT',
    data: { sheet_id }
  })
}

// 配置问卷
export function assignOfflineActivityPaper(id, paper_id) {
  return request({
    url: `/doctor/offline-activity/${id}/assign-questionnaire-paper`,
    method: 'PUT',
    data: { paper_id }
  })
}

// 生成分享url
export function generateActivityShareUri(id) {
  return request({
    url: `/doctor/offline-activity/${id}/gen-share-url`,
    method: 'GET'
  })
}

// 搜索线下活动可用报名表
export function searchValidSheet(keywords = '', limit = 5) {
  return request({
    url: `/doctor/offline-activity/search-valid-sheet`,
    method: 'GET',
    params: { keywords, limit }
  })
}

// 搜索线下活动可用问卷
export function searchValidPaper(keywords = '', limit = 5) {
  return request({
    url: `/doctor/offline-activity/search-valid-paper`,
    method: 'GET',
    params: { keywords, limit }
  })
}

// 删除报名表
export function delActivitySheet(id, sheet_id) {
  return request({
    url: `/doctor/offline-activity/${id}/sheet/${sheet_id}`,
    method: 'DELETE'
  })
}

// 删除问卷
export function delActivityPaper(id, paper_id) {
  return request({
    url: `/doctor/offline-activity/${id}/paper/${paper_id}`,
    method: 'DELETE'
  })
}

// 获取报表
export function getActivityReports(
  keywords = '',
  start = '',
  end = '',
  name = '',
  mobile = '',
  hospital_id = 0,
  depart_id = 0,
  level = '',
  title = '',
  current = 1,
  limit = 10
) {
  return request({
    url: '/doctor/offline-activity/reports',
    method: 'GET',
    params: {
      keywords,
      start,
      end,
      name,
      mobile,
      hospital_id,
      depart_id,
      level,
      title,
      current,
      limit
    }
  })
}

// 获取报表问卷数据
export function getActivityReportPaperAnswers(id) {
  return request({
    url: `/doctor/offline-activity/reports/${id}/paper-answers`,
    method: 'GET'
  })
}

// 导出报表
export function exportActivityReports(
  keywords = '',
  start = '',
  end = '',
  name = '',
  mobile = '',
  hospital_id = 0,
  depart_id = 0,
  level = '',
  title = ''
) {
  return request({
    url: '/doctor/offline-activity/reports/export',
    method: 'POST',
    data: {
      keywords,
      start,
      end,
      name,
      mobile,
      hospital_id,
      depart_id,
      level,
      title
    }
  })
}
