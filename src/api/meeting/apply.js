import request from '@/utils/request'

// 列表
export function listApply(
    applicant_id = 0,
    zone_id = 0,
    apply_start = "",
    apply_end = "",
    meeting_start = "",
    meeting_end = "",
    status = 0, 
    current = 1, 
    limit = 10,
    sort_start = 0
    ) {
    return request({
        url: "/meeting/apply",
        method: 'get',
        params: {
           applicant_id,
           zone_id,
           apply_start,
           apply_end,
           meeting_start,
           meeting_end,
           status,
           current,
           limit,
           sort_start
        }
    })
}

// 审核
export function audit(id, is_pass = false, reason = "") {
    return request({
        url: `/meeting/apply/${id}/audit`,
        method: 'put',
        data: {
            is_pass,
            reason
        }
    })
}

// 详情
export function detail(id) {
    return request({
        url: `/meeting/apply/${id}`,
        method: 'get'
    })
}

// 大区
export function getAreas() {
    return request({
        url: `/meeting/area`,
        method: 'get'
    })
}