import request from '@/utils/request'

// 任务类型.
export function taskTypes() {
     return request({
          url: "/tasks/types",
          method: 'get'
     })
}

// 任务列表.
export function listTask(type, current = 1, limit = 10) {
     return request({
          url: "/tasks",
          method: 'get',
          params: {
               type,
               current,
               limit
          }
     })
}

// 重新投递.
export function rePush(id) {
     return request({
          url: `/tasks/re-push/${id}`,
          method: 'post'
     })
}
