import request from '@/utils/request'

//搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/search',
    method: 'post',
    data
  })
}

//修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/update',
    method: 'post',
    data
  })
}

//删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/delete',
    method: 'post',
    data
  })
}

//置顶
export function top(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/top',
    method: 'post',
    data
  })
}

//取消置顶
export function cancelTop(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/cancelTop',
    method: 'post',
    data
  })
}

//上架
export function publish(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/publish',
    method: 'post',
    data
  })
}

//下架
export function out(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/out',
    method: 'post',
    data
  })
}

//详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/detail',
    method: 'post',
    data
  })
}

//排序
export function rank(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/rank',
    method: 'post',
    data
  })
}

//按视频名称模糊搜索
export function searchBySeriesName(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/searchBySeriesName',
    method: 'post',
    data
  })
}

//根据id搜索
export function searchByVideoId(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/searchByVideoId',
    method: 'post',
    data
  })
}

//索引同步
export function updateEs(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/es',
    method: 'post',
    data
  })
}
