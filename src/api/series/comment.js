import request from '@/utils/request'

//搜索直播名称
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/searchBySeriesName',
    method: 'get',
    data
  })
}

//获取评论
export function getComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/getCommentById',
    method: 'post',
    data
  })
}

//置顶
export function topComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/topComment',
    method: 'post',
    data
  })
}

//取消置顶
export function cancelTopComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/cancelTopComment',
    method: 'post',
    data
  })
}

//删除评论
export function deleteComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/deleteComment',
    method: 'post',
    data
  })
}

//添加评论
export function addComment(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/series/addAdminComment',
    method: 'post',
    data
  })
}
