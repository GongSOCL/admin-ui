import request from '@/utils/request'

// 流程类型
export function flowType() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/flowType',
    method: 'get'
  })
}

// 流程选项
export function works(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/works',
    method: 'post',
    data
  })
}

// 召回
export function callback(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/callback',
    method: 'post',
    data
  })
}

// 取消
export function cancel(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/cancel',
    method: 'post',
    data
  })
}

// 同意取消
export function agreeCancel(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/agreeCancel',
    method: 'post',
    data
  })
}

// 通过
export function pass(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/pass',
    method: 'post',
    data
  })
}

// 拒绝
export function reject(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/reject',
    method: 'post',
    data
  })
}

// 流程图
export function flowChart(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/flowChart',
    method: 'post',
    data
  })
}
