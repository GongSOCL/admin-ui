import request from '@/utils/request'

// 流程列表
export function meetingList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/list',
    method: 'post',
    data
  })
}

// 待处理流程列表
export function meetingVerifyList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/verifyList',
    method: 'post',
    data
  })
}

// 流程详情
export function meetingDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/detail',
    method: 'post',
    data
  })
}

// 流程详情修改
export function meetingUpdate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/update',
    method: 'post',
    data
  })
}

// 选择人员
export function users(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/users',
    method: 'post',
    data
  })
}

// 流程固定信息
export function staticInfo(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/staticInfo',
    method: 'post',
    data
  })
}

// 选择申请人员
export function applyUsers(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/workflow/meeting/applyUsers',
    method: 'post',
    data
  })
}
