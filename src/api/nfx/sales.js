import request from '@/utils/request'

// 上传销售数据任务用于奖金计算.
export function uploadSalesTask(
     uploadId, 
     uploadUrl, 
     currentYearStart = 1, 
     currentYearEnd = 1,
     lastYearStart = 1,
     lastYearEnd = 1 
) {
     return request({
          url: "/nfx/sales",
          method: 'put',
          data: {
              id: uploadId,
              url: uploadUrl,
              current_start_month: currentYearStart,
              current_end_month: currentYearEnd,
              last_start_month: lastYearStart,
              last_end_month: lastYearEnd
          }
     })
}