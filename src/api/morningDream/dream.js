import request from '@/utils/request'

export function dreamList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/list',
    method: 'get',
    params: data
  })
}

// 获取类型
export function getType() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/getType',
    method: 'get'
  })
}

// 获取类型
export function getDepartmen() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/getDepartmen',
    method: 'get'
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/edit',
    method: 'post',
    data
  })
}

// 删除
export function deleted(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/del',
    method: 'post',
    params: { 'id': id }
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/details',
    method: 'get',
    params: { 'id': id }
  })
}

export function soupList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/chicken_soup/list',
    method: 'get',
    params: data
  })
}

// 修改或新建
export function soupUpdate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/chicken_soup/edit',
    method: 'post',
    data
  })
}

// 删除
export function soupDeleted(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/chicken_soup/del',
    method: 'post',
    params: { 'id': id }
  })
}

// 详情
export function soupDetail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/chicken_soup/details',
    method: 'get',
    params: { 'id': id }
  })
}

// 分类
// 搜索
export function typeSearch(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/type/list',
    method: 'get',
    data
  })
}

// 修改或新建
export function typeUpdate(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/type/edit',
    method: 'post',
    data
  })
}

// 删除
export function typeDeleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/type/del',
    method: 'post',
    data
  })
}

// 详情
export function typeDetail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/type/details',
    method: 'get',
    params: { 'id': id }
  })
}

// 获取大区
export function getArea() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/getArea',
    method: 'get'
  })
}

// 获取省份
export function getProvince() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/getProvince',
    method: 'get'
  })
}

// 统计
export function rank(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/dream/rank',
    method: 'post',
    data
  })
}

// 统计导出
export const rankExport = process.env.VUE_APP_BASE_API + '/dream/rankExport'

// 人员导入
export const usersImport = process.env.VUE_APP_BASE_API + '/dream/users-u-l'

export const fileSize = 20
