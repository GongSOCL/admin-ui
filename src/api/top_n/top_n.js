import request from '@/utils/request'

export function listBatches(biz_id, type) {
    return request({
        url: "/top-point-rewards/batches",
        method: "GET",
        params: {
            biz_id,
            type
        }
    })
}

export function listQuestionnaireBatchDetail(id, current = 1, limit = 10) {
    return request({
        url: `/top-point-rewards/batches/${id}/questionnaire`,
        method: "GET",
        params: {
            current,
            limit
        }
    })
}

export function listExamBatchDetail(id, current = 1, limit = 10) {
  return request({
    url: `/top-point-rewards/batches/${id}/exam`,
    method: "GET",
    params: {
      current,
      limit
    }
  })
}

export function listTopicBatchDetail(id, current = 1, limit = 10) {
  return request({
    url: `/top-point-rewards/batches/${id}/topics`,
    method: "GET",
    params: {
      current,
      limit
    }
  })
}
