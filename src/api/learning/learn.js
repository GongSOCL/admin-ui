import request from '@/utils/request'

export function dt_list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/dt_list',
    method: 'get',
  })
}

export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/list',
    method: 'get',
    params: data
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/activity_list',
    method: 'post',
    data
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/edit',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/activity_del',
    method: 'post',
    data
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/dtl',
    method: 'get',
    params: { 'id': id }
  })
}
// 详情
export function check_st(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/check_st',
    method: 'post',
    data
  })
}


// 获取学习计划下用户资料
export function distribution_after(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/distribution_after',
    method: 'get',
    params:data
  })
}

// 关闭用户学习计划
export function close_user_materials(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/learn/close_user_materials',
    method: 'post',
    data
  })
}

// 部门和下面的所有人 + 回显列表
export function getDepartmentsUsers() {
  return request({
    url: `/learn/power_list`,
    method: 'get'
  })
}


// 权限提交
export function PowerEdit(data) {
  return request({
    url: `/learn/power_edit`,
    method: 'post',
    data
  })
}


// 权限
export function getDepartmentsUsersDetail(id) {
  return request({
    url: `/learn/power_dtl`,
    method: 'get',
    params: { 'id': id }
  })
}

// 考试列表
export function getExamList(date_type) {
  return request({
    url: `/learn/exam-list`,
    method: 'get',
    params: { 'date_type': date_type }
  })
}
export const fileSize = 20
