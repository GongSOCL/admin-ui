import request from '@/utils/request'


// 获取类型
export function getType() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/getType',
    method: 'get'
  })
}

// 获取部门
export function getDepartment() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/getDepartment',
    method: 'get'
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/search',
    method: 'post',
    data
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/update',
    method: 'post',
    data
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/details',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/del',
    method: 'post',
    data
  })
}

// 内容统计
export function rankByPic(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/rankByPic',
    method: 'post',
    data
  })
}

// 用户统计
export function rankByUser(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/morning/share/rankByUser',
    method: 'post',
    data
  })
}

// 内容导出
export const rankByPicExport = process.env.VUE_APP_BASE_API + '/morning/share/rankByPicExport'

// 用户导出
export const rankByUserExport = process.env.VUE_APP_BASE_API + '/morning/share/rankByUserExport'
