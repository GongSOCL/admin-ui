import request from '@/utils/request'

// 查询列表
export function listCategory(query) {
  return request({
    url: '/doctor/share/article/category',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getCategoryDt(id) {
  return request({
    url: `/doctor/share/article/category/dtl/${id}`,
    method: 'get'
  })
}

// 修改
export function updateCategory(data) {
  return request({
    url: `/doctor/share/article/category/edit`,
    method: 'post',
    data: data
  })
}

// 删除
export function delCategory(id) {
  return request({
    url: `/doctor/share/article/category/del/${id}`,
    method: 'post'
  })
}

// ----------------- 文章 ----------------- //
// 查询列表
export function listArticle(query) {
  return request({
    url: '/doctor/share/article',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getArticleDt(id) {
  return request({
    url: `/doctor/share/article/dtl/${id}`,
    method: 'get'
  })
}

// 修改
export function updateArticle(data) {
  return request({
    url: `/doctor/share/article/edit`,
    method: 'post',
    data: data
  })
}

// 删除
export function delArticle(id) {
  return request({
    url: `/doctor/share/article/del/${id}`,
    method: 'post'
  })
}

// 主分类
export function getCategoryMain() {
  return request({
    url: `/doctor/share/article/category-top-level`,
    method: 'get'
  })
}

// 子分类
export function getCategory(id) {
  return request({
    url: `/doctor/share/article/category-top-level/${id}`,
    method: 'get'
  })
}

// 获取关联文章
export function getBindArticle(id) {
  return request({
    url: `/doctor/share/article/get-bind-article/${id}`,
    method: 'get'
  })
}

// 关联文章
export function bindArticle(id, data) {
  return request({
    url: `/doctor/share/article/bind-article/${id}`,
    method: 'post',
    data: data
  })
}
export const fileSize = 20

export const toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'], // 加粗 斜体 下划线 删除线 -----['bold', 'italic', 'underline', 'strike']
  ['blockquote', 'code-block'], // 引用  代码块-----['blockquote', 'code-block']
  [{
    header: 1
  }, {
    header: 2
  }], // 1、2 级标题-----[{ header: 1 }, { header: 2 }]
  [{
    list: 'ordered'
  }, {
    list: 'bullet'
  }], // 有序、无序列表-----[{ list: 'ordered' }, { list: 'bullet' }]
  [{
    script: 'sub'
  }, {
    script: 'super'
  }], // 上标/下标-----[{ script: 'sub' }, { script: 'super' }]
  [{
    indent: '-1'
  }, {
    indent: '+1'
  }], // 缩进-----[{ indent: '-1' }, { indent: '+1' }]
  [{
    'direction': 'rtl'
  }], // 文本方向-----[{'direction': 'rtl'}]
  [{
    size: ['12px', '14px', '16px', '20px', '24px', '36px']
  }], // 字体大小-----[{ size: ['small', false, 'large', 'huge'] }]
  [{
    header: [1, 2, 3, 4, 5, 6, false]
  }], // 标题-----[{ header: [1, 2, 3, 4, 5, 6, false] }]
  [{
    color: []
  }, {
    background: []
  }], // 字体颜色、字体背景颜色-----[{ color: [] }, { background: [] }]
  [{
    font: []
  }], // 字体种类-----[{ font: [] }]
  [{
    align: []
  }], // 对齐方式-----[{ align: [] }]
  ['clean'], // 清除文本格式-----['clean']
  ['link', 'image', 'video'] // 链接、图片、视频-----['link', 'image', 'video']
]
