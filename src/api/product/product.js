import request from '@/utils/request'

export function getProduct(show_only = 0) {
     return request({
          url: "/products",
          method: 'get',
          params: { show_only }
     })
}
