import request from '@/utils/request'

export function getHospital(data) {
  return request({
    url: '/hospital/getHospital',
    method: 'post',
    data
  })
}

// 搜索返回一定数量医院， 走es,数据可能有延迟
export function searchHospitalLimited(keywords = '', limit = 5) {
  return request({
    url: '/hospital/search-limited',
    method: 'GET',
    params: { keywords, limit }
  })
}

// 获取公司
export function company() {
  return request({
    url: '/hospital/company',
    method: 'get'
  })
}

// 医院列表
export function hospitalList(data) {
  return request({
    url: '/hospital/list',
    method: 'post',
    data
  })
}

// 复制医院
export function copy(data) {
  return request({
    url: '/hospital/copy',
    method: 'post',
    data
  })
}
