import request from '@/utils/request'

export function getCompany() {
  return request({
    url: '/import/getCompany',
    method: 'get'
  })
}

export function getImportType() {
  return request({
    url: '/import/getImportType',
    method: 'get'
  })
}

// 导入
export const importAll = process.env.VUE_APP_BASE_API + '/import/import'

// 任务列表
export const getImportTask = process.env.VUE_APP_BASE_API + '/import/getImportTask'


// 拜访记录重新校验
export function visitGpsReCheck(id, url) {
  return request({
    url: '/import/visit-gps-recheck',
    method: 'post',
    data: {
      id,
      url
    }
  })
}

export function checkReps(data) {
  return request({
    url: '/import/checkReps',
    method: 'post',
    data
  })
}

export const fileSize = 20

export function visitGpsReFake(id, url) {
  return request({
    url: '/import/visit-gps-fake',
    method: 'post',
    data: {
      id,
      url
    }
  })
}

/**
 * 获取药店的大区信息
 * @returns {*}
 */
export function getStoreArea() {
  return request({
    url: '/agent/situation/store-area',
    method: 'get'
  })
}

export function getstoreProvince(area_id = 0) {
  return request({
    url: '/agent/situation/store-province?area_id='+area_id,
    method: 'get'
  })
}

export function getstoreCity(province_id = 0) {
  return request({
    url: '/agent/situation/store-city?province_id='+province_id,
    method: 'get'
  })
}

export function getmainStore(city_id = 0) {
  return request({
    url: '/agent/situation/store-main?city_id='+city_id,
    method: 'get'
  })
}

export function getStore(main_store_id = 0) {
  return request({
    url: '/agent/situation/store-list?main_store_id='+main_store_id,
    method: 'get'
  })
}

export function getStoreProduct(store_id = 0) {
  return request({
    url: '/agent/situation/store-product?store_id='+store_id,
    method: 'get'
  })
}

export function getStoreProductSku(product_id = 0) {
  return request({
    url: '/agent/situation/store-product-sku?product_id='+product_id,
    method: 'get'
  })
}

// getStoreProductSku
