import request from '@/utils/request'

export function getDepart(data) {
  return request({
    url: '/depart/getDepart',
    method: 'post',
    data
  })
}

export function searchDepart(keywords = '', limit = 5) {
  console.log(keywords)
  return request({
    url: '/depart/search',
    method: 'GET',
    params: { keywords, limit }
  })
}
