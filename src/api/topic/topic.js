import request from '@/utils/request'

// 搜索话题
export function searchTopic(keywords = '', limit = 10) {
  return request({
    url: '/topics/search',
    method: 'GET',
    params: {
      keywords,
      limit
    }
  })
}

// 列表话题
export function listTopic(current = 1, limit) {
  return request({
    url: '/topics',
    method: 'GET',
    params: {
      current,
      limit
    }
  })
}

// 添加话题
export function addTopic(name, cover_id, detail_id, is_publish = 1) {
  return request({
    url: '/topics',
    method: 'POST',
    data: {
      name,
      cover_id,
      detail_id,
      is_publish
    }
  })
}

// 编辑话题
export function editTopic(id, name, cover_id, detail_id, is_publish = 1) {
  return request({
    url: `/topics/${id}`,
    method: 'PUT',
    data: {
      name,
      cover_id,
      detail_id,
      is_publish
    }
  })
}

// 删除话题
export function delTopic(id) {
  return request({
    url: `/topics/${id}`,
    method: 'DELETE'
  })
}

// 修改状态
export function status(id, status) {
  return request({
    url: `/topics/${id}/status`,
    method: 'PUT',
    data: {
      status
    }
  })
}

// 话题详情
export function infoTopic(id) {
  return request({
    url: `/topics/${id}`,
    method: 'GET'
  })
}
