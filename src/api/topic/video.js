import request from '@/utils/request'

//列表
export function listVideos(
    user_id = 0, 
    topic_id = 0, 
    start = "", 
    end = "", 
    label_id = 0, 
    status = 0, 
    current = 1, 
    limit = 10
    ) {
    return request({
        url: "/topic/videos",
        method: "GET",
        params: {
            user_id,
            topic_id,
            start,
            end,
            label_id,
            status,
            current,
            limit
        }
    })
}

//详情
export function infoVideo(id, source) {
    return request({
        url: `/topic/videos/${id}`,
        method: "GET",
        params: {
            source: source
        }
    })
}

//审核通过
export function auditSuccess(id, label_ids = []) {
    return request({
        url: `/topic/videos/${id}/audit-success`,
        method: "PUT",
        data: {
            label_ids
        }
    })
}

//审核通过
export function auditFail(id, reason = "") {
    return request({
        url: `/topic/videos/${id}/audit-fail`,
        method: "PUT",
        data: {
            reason
        }
    })
}

//审核通过
export function listLabels() {
    return request({
        url: `/topic/videos/labels`,
        method: "GET",
    })
}

//审核通过
export function deleteVideo(id) {
    return request({
        url: `/topic/videos/${id}`,
        method: "DELETE",
    })
}