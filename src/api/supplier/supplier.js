import request from '@/utils/request'

export function getProductsForSelect() {
  return request({
    url: '/supplier/getProductsForSelect',
    method: 'get'
  })
}

export function searchHospital(data) {
  return request({
    url: '/supplier/searchHospital',
    method: 'post',
    data
  })
}

export function getServiceType() {
  return request({
    url: '/supplier/getServiceType',
    method: 'get'
  })
}

export function getAreaList() {
  return request({
    url: '/supplier/getAreaList',
    method: 'get'
  })
}

export function checkPhone(data) {
  return request({
    url: '/supplier/checkPhone',
    method: 'post',
    data
  })
}

export function searchSupplier(data) {
  return request({
    url: '/supplier/searchSupplier',
    method: 'post',
    data
  })
}

export function getSupplierHospital(data) {
  return request({
    url: '/supplier/getSupplierHospital',
    method: 'post',
    data
  })
}

export function deleteSupplier(data) {
  return request({
    url: '/supplier/deleteSupplier',
    method: 'post',
    data
  })
}

export function createOrUpdate(data) {
  return request({
    url: '/supplier/createOrUpdate',
    method: 'post',
    data
  })
}

export function getDetail(data) {
  return request({
    url: '/supplier/getDetail',
    method: 'post',
    data
  })
}

export function copyOne(data) {
  return request({
    url: '/supplier/copy',
    method: 'post',
    data
  })
}
