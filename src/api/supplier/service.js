import request from '@/utils/request'

export function searchServiceList(data) {
  return request({
    url: '/supplier/searchServiceList',
    method: 'post',
    data
  })
}

export function passOrRefuse(data) {
  return request({
    url: '/supplier/passOrRefuse',
    method: 'post',
    data
  })
}

export function getSupplierInfo(data) {
  return request({
    url: '/supplier/getSupplierInfo',
    method: 'post',
    data
  })
}

export function exportServerInfo(data) {
  return request({
    url: '/supplier/exportServerInfo',
    method: 'post',
    data
  })
}
