import request from '@/utils/request'

export function searchVisit(data) {
  return request({
    url: '/supplier/searchVisit',
    method: 'post',
    data
  })
}

export function exportVisit(data) {
  return request({
    url: '/supplier/exportVisit',
    method: 'post',
    data
  })
}
