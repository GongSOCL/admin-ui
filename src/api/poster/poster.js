import request from '@/utils/request'

//直播详情
export function projectList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/projectList',
    method: 'post',
    data
  })
}


//直播详情
export function projectDetail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/projectDetail',
    method: 'post',
    data
  })
}

//搜索
export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/list',
    method: 'post',
    data
  })
}

//修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/update',
    method: 'post',
    data
  })
}

//删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/delete',
    method: 'post',
    data
  })
}

//详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/detail',
    method: 'post',
    data
  })
}

//上架
export function publishPoster(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/publishPoster',
    method: 'post',
    data
  })
}

//下架
export function outPoster(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/outPoster',
    method: 'post',
    data
  })
}

//下架
export function copyAndParse(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/copyAndParse',
    method: 'post',
    data
  })
}


//产品
export function drug_series(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/poster/drug_series',
    method: 'get',
  })
}
