import request from '@/utils/request'

export function listInvite(paperId) {
  return request({
    url: `/questionnaires/${paperId}/invite`,
    method: 'get'
  })
}

export function addInvite(paperId, userId, userType, quota) {
  return request({
    url: `/questionnaires/${paperId}/invite`,
    method: 'put',
    data: {
      user_id: userId,
      user_type: userType,
      quota: quota
    }
  })
}

export function editInivte(paperId, id, quota) {
  return request({
    url: `/questionnaires/${paperId}/invite/${id}`,
    method: 'post',
    data: {
      quota: quota
    }
  })
}

export function infoInvite(paperId, id) {
  return request({
    url: `/questionnaires/${paperId}/invite/${id}`,
    method: 'get'
  })
}

export function deleteInvite(paperId, id) {
  return request({
    url: `/questionnaires/${paperId}/invite/${id}`,
    method: 'delete'
  })
}

export function listValidUsers(paperId, userType = 1, limit = 10, keywords = '') {
  return request({
    url: `/questionnaires/${paperId}/invite/valid-agent`,
    method: 'get',
    params: {
      'paper_id': paperId,
      'user_type': userType,
      'limit': limit,
      'keywords': keywords
    }
  })
}
