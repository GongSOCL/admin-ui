import request from '@/utils/request'

// 选项列表
export function listOptions(questionId) {
  return request({
    url: `/logic/options/list/${questionId}`,
    method: 'get'
  })
}

// 添加选项
export function addOptions(questionId, name, isRight = false) {
  return request({
    url: `/logic/options`,
    method: 'put',
    data: {
      question_id: questionId,
      name: name,
      is_right: isRight
    }
  })
}

// 编辑选项
export function editOptions(optionId, name, isRight = false) {
  return request({
    url: `/logic/options/${optionId}`,
    method: 'post',
    data: {
      name: name,
      is_right: isRight
    }
  })
}

// 删除选项
export function deleteOptions(optionId) {
  return request({
    url: `/logic/options/${optionId}`,
    method: 'delete'
  })
}
