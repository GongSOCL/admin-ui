import request from '@/utils/request'

// 试题列表
export function listLogicQuestions(paperId) {
  return request({
    url: `/logic/question/list/${paperId}`,
    method: 'get'
  })
}
// 添加问卷
export function addLogicQuestions(paperId, name, type, options = [], isSkip=false) {
  return request({
    url: `/logic/question`,
    method: 'put',
    data: {
      paper_id: paperId,
      name: name,
      type: type,
      options: options,
      is_skip: isSkip ? 1 : 0
    }
  })
}

// 编辑问卷
export function editLogicQuestions(questionId, name, type, options = [], isSkip = false) {
  return request({
    url: `/logic/question/${questionId}`,
    method: 'post',
    data: {
      name: name,
      type: type,
      options: options,
      is_skip: isSkip ? 1 : 0
    }
  })
}

// 删除问卷
export function deleteLogicQuestions(questionId) {
  return request({
    url: `/logic/question/${questionId}`,
    method: 'delete'
  })
}

// 排序问卷
export function sortLogicQuestions(questionId, prev_id = 0, next_id = 0) {
  return request({
    url: `/logic/question/sort/${questionId}`,
    method: 'post',
    data: {
      prev_id,
      next_id
    }
  })
}

// 获取可用下一题目
export function getValidNextQuestion(questionId) {
  return request({
    url: `/logic/question/next/${questionId}`,
    method: 'get'
  })
}

