import request from '@/utils/request'

// 规则列表
export function listRules(questionId) {
  return request({
    url: `/questionnaire/rules/${questionId}`,
    method: 'get'
  })
}

// 批量更新规则
export function updateRules(questionId, rules = []) {
  return request({
    url: `/questionnaire/rules/${questionId}`,
    method: 'post',
    data: {
      'rules': rules
    }
  })
}

// 删除规则
export function deleteRules(ruleId, rules = []) {
  return request({
    url: `/questionnaire/rules/${ruleId}`,
    method: 'post',
    data: [
      rules
    ]
  })
}
