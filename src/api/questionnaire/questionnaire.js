import request from '@/utils/request'

// 问卷列表
export function listPaper(status = 0, name = '', current = 1) {
  return request({
    url: '/questionnaires',
    method: 'get',
    params: {
      status,
      name,
      current
    }
  })
}

// 添加问卷
export function addPaper(name, type, platform, auto_publish_platform, show_type) {
  return request({
    url: '/questionnaires',
    method: 'put',
    data: {
      name,
      type,
      platform,
      auto_publish_platform,
      show_type
    }
  })
}

// 问卷详情
export function infoPaper(id) {
  return request({
    url: '/questionnaires/' + id,
    method: 'get'
  })
}

// 发布问卷
export function publishPaper(id) {
  return request({
    url: '/questionnaires/publish/' + id,
    method: 'post'
  })
}

// 删除问卷
export function deletePaper(id) {
  return request({
    url: '/questionnaires/' + id,
    method: 'delete'
  })
}

// 下架问卷
export function offShelfPaper(id) {
  return request({
    url: '/questionnaires/off-shelf/' + id,
    method: 'post'
  })
}

// 更新基本信息
export function updateBaseInfo(id, name, total, expire_at = '', intro = '', startBgId=0, startBgIds = [], tailBgId = 0, tailBgIds = [], answerBgId = 0, answerBgIds = [], is_invite_answer = 0, invite_expire_times = 0) {
  return request({
    url: '/questionnaires/info/base/' + id,
    method: 'post',
    data: {
      name,
      total,
      expire_at,
      intro,
      start_bg_id: startBgId,
      start_bg_ids: startBgIds,
      tail_bg_id: tailBgId,
      tail_bg_ids: tailBgIds,
      answer_bg_id: answerBgId,
      answer_bg_ids: answerBgIds,
      is_invite_answer: is_invite_answer,
      invite_expire_times: invite_expire_times
    }
  })
}

// 更新邀请状态信息
export function updateInvitePaper(id, isInviteToAnswer, inviteExpireHours = 0) {
  return request({
    url: '/questionnaires/info/invite-answer/' + id,
    method: 'post',
    data: {
      id: id,
      is_invite_to_answer: isInviteToAnswer ? 1 : 0,
      expire_hours: inviteExpireHours
    }
  })
}

export function getBg(id, type) {
  return request({
    url: `/questionnaires/bg/${id}/${type}`,
    method: 'get'
  })
}

export function generateShareUri(id) {
  return request({
    url: `/questionnaires/share-uri/${id}`,
    method: 'get'
  })
}

export const questionnaire_answer_exp = process.env.VUE_APP_BASE_API + '/questionnaires/down-answer'

export function sendTopNPointRewards(id, upload_id) {
  return request({
    url: `/questionnaires/${id}/send-top-point-rewards`,
    method: "POST",
    data: {
      upload_id
    }
  })
}
export function updatePaperPointInfo(
  paper_id,
  player_platform,
  player_group_id,
  player_service_point,
  player_money_point,
  share_group_id,
  share_user_service_point,
  share_user_money_point,
  top_n_service_point = 0,
  top_n_money_point = 0
) {
  return request({
    url: `/questionnaires/${paper_id}/point`,
    method: 'PUT',
    data: {
      player_platform,
      player_group_id,
      player_service_point,
      player_money_point,
      share_group_id,
      share_user_service_point,
      share_user_money_point,
      top_n_money_point,
      top_n_service_point
    }
  })
}
