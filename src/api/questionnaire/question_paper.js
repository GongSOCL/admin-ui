import request from '@/utils/request'

// 关联试卷列表
export function listQuestionPaper(paperId) {
  return request({
    url: `/question-paper/list/${paperId}`,
    method: 'get'
  })
}

// 删除问卷关联试卷
export function deleteQuestionPaper(paperId, questionPaperId) {
  return request({
    url: `/question-paper/${paperId}/${questionPaperId}`,
    method: 'delete'
  })
}

// 查找问卷可关联试卷
export function searchQuestionPaper(paperId, keywords = '', limit = 10) {
  return request({
    url: `/question-paper/search`,
    method: 'post',
    data: {
      paper_id: paperId,
      keywords: keywords,
      limit: limit
    }
  })
}

// 批量添加问卷试卷
export function batchAddQuestionPaper(paperId, questionPaperIds) {
  return request({
    url: `/question-paper/${paperId}`,
    method: 'put',
    data: {
      paper_id: paperId,
      ids: questionPaperIds
    }
  })
}

// 获取问卷所有关联试题及答案
export function previewQuestionnaireQuestion(paperId) {
  return request({
    url: `/question-paper/preview/${paperId}`,
    method: 'get'
  })
}

// 问卷试题绑定资源
export function questionnaireQuestionBindResource(paperId, question_id, resource_id = 0) {
  return request({
    url: `/question-paper/${paperId}/bind-resource`,
    method: 'put',
    data: {
      question_id,
      resource_id
    }
  })
}

// 搜索可绑定资源
export function searchQuestionnaireQuestionResources(keywords = "", limit = 10, platform) {
  return request({
    url: `/question-paper/otc-resource`,
    method: 'get',
    params: {
      keywords,
      limit,
      platform
    }
  })
}