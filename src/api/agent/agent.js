import request from '@/utils/request'

export function getAgentByServeProduct(productId, current = 1, limit = 10, keywords = "") {
     return request({
          url: `/agent/server/product/${productId}`,
          method: 'get',
          params: {
               current,
               limit,
               keywords
          }
        })
}

//代表搜索
export function search(data) {
  return request({
    url: '/agent/search',
    method: 'post',
    data
  })
}

//代表导出
export const agentExport = process.env.VUE_APP_BASE_API + '/agent/export'
