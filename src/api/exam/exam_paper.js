import request from '@/utils/request'

// 获取考试可用试卷
export function getExamValidTestPaper(query) {
    return request({
        url: `/compliance/exams/exam-paper`,
        method: 'get',
        params: {
            page_name:query.keywords,
            question_type_status:query.question_type_status,
            test_paper_type_status:query.test_paper_type_status,
            start_time:query.start_time,
            page: query.page,
            page_size: query.page_size,
        }
    })
}

// 试卷下题目
export function getTestPaperItem(id, question_type, test_paper_type) {
    return request({
        url: `/compliance/exams/exam-paper-info`,
        method: 'get',
        params: {
            page_id: id,
            t: question_type,
            test_t: test_paper_type
        }
    })
}

// 试卷下题目
export function examPaperEdit(itemsData, type, question_type, test_id, sub_source=0, cc=0) {
    return request({
        url: `/compliance/exams/exam-paper-edit`,
        method: 'get',
        params: {
            item_source_list: itemsData,
            t: question_type,
            type: type,
            test_id: test_id,
            sub_source: sub_source,
            cc: cc
        }
    })
}
