import request from '@/utils/request'

// 考试列表
export function listExams(keywords = '', status = 0, current = 1, limit = 10) {
  return request({
    url: '/compliance/exams',
    method: 'get',
    params: {
      keywords,
      status,
      current,
      limit
    }
  })
}

// 添加考试
export function addExams(name, start, end = '', intro = '', cash = 0, point = 0,
  is_publish_answer = false, is_wrong_remind = false, test_paper_id = 0, pass = 0,
  users = [], users_excel = [], item_source_list, pass_type, pass_fraction, users_file_path,
  up_exam_end, up_exam_start,
  up_exam_num,
  up_exam_time,
  exam_num,
  exam_time,
  is_review,
  exam_type,
  plan_days,
  platform,
  group_id = 0,
  top_n_service_point = 0,
  top_n_money_point = 0) {
  return request({
    url: '/compliance/exams',
    method: 'put',
    data: {
      name,
      start,
      end,
      intro,
      cash,
      point,
      is_publish_answer,
      is_wrong_remind,
      test_paper_id,
      pass,
      users,
      users_excel,
      item_source_list,
      pass_type,
      pass_fraction,
      users_file_path,
      up_exam_end,
      up_exam_start,
      up_exam_num,
      up_exam_time,
      exam_num,
      exam_time,
      is_review,
      exam_type,
      plan_days,
      platform,
      group_id,
      top_n_service_point,
      top_n_money_point
    }
  })
}

// 编辑考试
export function editExams(id, name, start, end = '', intro = '', cash = 0, point = 0, is_publish_answer = false,
  is_wrong_remind = false, test_paper_id = 0,
  pass = 0, users = [], users_excel = [],
  item_source_list, pass_type,
  pass_fraction, users_file_path,
  up_exam_end, up_exam_start,
  up_exam_num,
  up_exam_time,
  exam_num,
  exam_time,
  is_review,
  exam_type,
  plan_days,
  platform = 0,
  group_id = 0,
  top_n_service_point = 0,
  top_n_money_point = 0) {
  return request({
    url: `/compliance/exams/${id}`,
    method: 'post',
    data: {
      name,
      start,
      end,
      intro,
      cash,
      point,
      is_publish_answer,
      is_wrong_remind,
      test_paper_id,
      pass,
      users,
      users_excel,
      item_source_list,
      pass_type,
      pass_fraction,
      users_file_path,
      up_exam_end,
      up_exam_start,
      up_exam_num,
      up_exam_time,
      exam_num,
      exam_time,
      is_review,
      exam_type,
      plan_days,
      platform,
      group_id,
      top_n_service_point,
      top_n_money_point
    }
  })
}

// 考试详情
export function infoExam(id) {
  return request({
    url: `/compliance/exams/${id}`,
    method: 'get'
  })
}

// 获取考试可用试卷
export function getExamValidTestPaper(id, keywords = '', limit = 10) {
  return request({
    url: `/compliance/exams/${id}/test-paper`,
    method: 'get',
    params: {
      keywords,
      limit
    }
  })
}

// 删除考试
export function deleteExam(id) {
  return request({
    url: `/compliance/exams/${id}`,
    method: 'delete'
  })
}

// 结束考试.
export function finishExam(id) {
  return request({
    url: `/compliance/exams/end/${id}`,
    method: 'post'
  })
  // return request({
  //      url: `/compliance/exams/${id}/finish`,
  //      method: 'delete',
  // })
}

// 导出考试结果.
export function exportExam(id) {
  return request({
    url: `/compliance/exams/${id}/export-results`,
    method: 'put'
  })
}

// 考试列表
export function setAgreement(id, agreement_id) {
  return request({
    url: `/compliance/exams/${id}/agreement`,
    method: 'post',
    data: {
      agreement_id
    }
  })
}

// 试卷下题目
export function getExamItem(id, exid) {
  return request({
    url: `/compliance/exams/${id}/getExamItem`,
    method: 'get',
    params: {
      exid
    }
  })
}

// 试卷下题目
export function testCof(exid) {
  return request({
    url: `/compliance/exams/test_cof`,
    method: 'get',
    params: {
      test_id: exid
    }
  })
}

// 用户考试信息（新）
export function getUsersExamList(data) {
  console.log(data)
  return request({
    url: `/compliance/exams/users-e-lt`,
    method: 'get',
    params: {
      exam_name: data.exam_name,
      start_date: data.start_date,
      end_date: data.end_date,
      type: data.type,
      status: data.status,
      page: data.page,
      page_size: data.page_size
    }
  })
}

//
export function editUpExamDate(id) {
  return request({
    url: `/compliance/exams/edit-up-exam-dt`,
    method: 'get',
    params: {
      id: id
    }
  })
}
export function editUpExamDateSub(data) {
  return request({
    url: `/compliance/exams/edit-up-exam-dt`,
    method: 'post',
    data
  })
}

export function performance(year, month, exam_name, page, page_size) {
  return request({
    url: `/compliance/exams/exam_noun_summary`,
    method: 'get',
    params: {
      year: year,
      month: month,
      exam_name: exam_name,
      page: page,
      page_size: page_size
    }
  })
}

export function exam_statis_fraction(year, month, exam_name) {
  return request({
    url: `/compliance/exams/exam_statis_fraction`,
    method: 'get',
    params: {
      year: year,
      month: month,
      exam_name: exam_name
    }
  })
}

export function exam_statis_per(year, month, exam_name) {
  return request({
    url: `/compliance/exams/exam_statis_per`,
    method: 'get',
    params: {
      year: year,
      month: month,
      exam_name: exam_name
    }
  })
}

export function exam_detail(year, month, exam_name, page, page_size) {
  return request({
    url: `/compliance/exams/exam_detail`,
    method: 'get',
    params: {
      year: year,
      month: month,
      exam_name: exam_name,
      page: page,
      page_size: page_size
    }
  })
}

export const exam_performance_exp = process.env.VUE_APP_BASE_API + '/compliance/exams/exam_performance_exp'
export const exam_statis_fraction_exp = process.env.VUE_APP_BASE_API + '/compliance/exams/exam_statis_fraction_exp'
export const exam_statis_per_exp = process.env.VUE_APP_BASE_API + '/compliance/exams/exam_statis_per_exp'
export const exam_detail_exp = process.env.VUE_APP_BASE_API + '/compliance/exams/exam_detail_exp'

// 导出
export const userExport = process.env.VUE_APP_BASE_API + '/compliance/exams/user_export'
export const passExport = process.env.VUE_APP_BASE_API + '/compliance/exams/pass_export'

export const fileSize = 20

// 下载模版
export const downloadTmp = process.env.VUE_APP_BASE_API + '/compliance/exams/downloadExamsUserListTemplate'

// 上传白名单
export const examsUsersListExp = process.env.VUE_APP_BASE_API + '/compliance/exams/read-exams-u-l'

export function sendExamTopPointRewards(id, upload_id) {
  return request({
    url: `/compliance/exams/${id}/send-top-point-rewards`,
    method: 'POST',
    data: {
      upload_id
    }
  })
}
