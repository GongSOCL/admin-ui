import request from '@/utils/request'

// 协议列表
export function listAgreements(keywords = "", current = 1, limit = 10) {
    return request({
         url: "/compliance/agreements",
         method: 'get',
         params: {
              keywords,
              current,
              limit
         }
    })
}


// 添加协议
export function addAgreement(name, content) {
    return request({
         url: "/compliance/agreements",
         method: 'put',
         data: {
             name,
             content
         }
    })
}

// 编辑协议
export function editAgreement(id, name, content) {
    return request({
         url: `/compliance/agreements/${id}`,
         method: 'post',
         data: {
             name,
             content
         }
    })
}

// 删除协议
export function delAgreement(id) {
    return request({
         url: `/compliance/agreements/${id}`,
         method: 'delete'
    })
}


// 协议详情
export function infoAgreement(id) {
    return request({
         url: `/compliance/agreements/${id}`,
         method: 'get'
    })
}

// 搜索协议
export function searchAgreement(keywords) {
    return request({
         url: `/compliance/agreements/search`,
         method: 'get',
         params: {
             keywords
         }
    })
}
