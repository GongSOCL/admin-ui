import request from '@/utils/request'

// 获取大区
export function getArea() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/get-area',
    method: 'get'
  })
}

// 获取省份
export function getProvince() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/get-province',
    method: 'get'
  })
}

// 获取城市
export function getCity() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/get-city',
    method: 'get'
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/search-branch-store-list',
    method: 'post',
    data
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/detail',
    method: 'post',
    data
  })
}

// 编辑
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/update-gps',
    method: 'post',
    data
  })
}

// gps
export function autoGetGps(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/aerogen/gps/query-gps',
    method: 'post',
    data
  })
}
