import request from '@/utils/request'

//获取直播类型
export function getList(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/userAuth/list',
    method: 'post',
    data
  })
}

//获取分析直播类型
export function changeVerifyState(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/userAuth/changeVerifyState',
    method: 'post',
    data
  })
}
