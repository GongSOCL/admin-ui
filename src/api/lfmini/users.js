import request from '@/utils/request'

export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/users_list',
    method: 'get',
    params: data
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/users_list',
    method: 'post',
    data
  })
}

// 修改或新建
export function users_audit(uid, t, verify_reason, mobile_num, user_auth_id, area_id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/users_audit',
    method: 'post',
    data: {'uid':uid, 't':t, 'verify_reason':verify_reason,'mobile_num':mobile_num, 'user_auth_id':user_auth_id, 'area_id':area_id}
  })
}


// 详情
export function detail(id, user_verified) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/users_dtl',
    method: 'get',
    params: { 'id': id, 'user_verified':user_verified }
  })
}

// 搜索
export function limitSearch(keywords = "", limit = 10) {
  return request({
    url: '/lfmini/users/search',
    method: 'GET',
    params: {
      keywords,
      limit
    }
  })
}

export const fileSize = 20
