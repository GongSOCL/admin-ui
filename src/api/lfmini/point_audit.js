import request from '@/utils/request'

export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/point_list',
    method: 'get',
    params: data
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/point_list',
    method: 'post',
    data
  })
}

// 审核
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/point_audit',
    method: 'post',
    data
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/point_dtl',
    method: 'get',
    params: { 'id': id }
  })
}

export const fileSize = 20
