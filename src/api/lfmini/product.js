import request from '@/utils/request'

export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_list',
    method: 'get',
    params: data
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_list',
    method: 'post',
    data
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_edit',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_del',
    method: 'post',
    data
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_dtl',
    method: 'get',
    params: { 'id': id }
  })
}


// 宣传册
export function listDa(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_d_list',
    method: 'get',
    params: data
  })
}

// 修改或新建
export function updateDa(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_d_edit',
    method: 'post',
    data
  })
}

// 删除
export function deletedDa(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_d_del',
    method: 'post',
    data
  })
}

// 详情
export function detailDa(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_d_dtl',
    method: 'get',
    params: { 'id': id }
  })
}



// 说明书
export function listIns(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_ins_list',
    method: 'get',
    params: data
  })
}

// 修改或新建
export function updateIns(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_ins_edit',
    method: 'post',
    data
  })
}

// 删除
export function deletedIns(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_ins_del',
    method: 'post',
    data
  })
}

// 详情
export function detailIns(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/product_ins_dtl',
    method: 'get',
    params: { 'id': id }
  })
}
export const fileSize = 20
