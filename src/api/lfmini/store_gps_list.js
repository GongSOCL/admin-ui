import request from '@/utils/request'

// 获取大区
export function getArea() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/getArea',
    method: 'get'
  })
}

// 获取省份
export function getProvince() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/getProvince',
    method: 'get'
  })
}

// 获取城市
export function getCity() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/getCity',
    method: 'get'
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/searchList',
    method: 'post',
    data
  })
}

// 详情
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/detail',
    method: 'post',
    data
  })
}

// 编辑
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/update',
    method: 'post',
    data
  })
}

// gps
export function autoGetGps(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/gps/autoGetGps',
    method: 'post',
    data
  })
}
