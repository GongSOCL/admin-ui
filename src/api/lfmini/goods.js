import request from '@/utils/request'

// 搜索
export function search(status, page, page_size) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_list',
    method: 'get',
    params: {
      status:status,
      page: page,
      page_size: page_size
    }
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_edit',
    method: 'post',
    data
  })
}

// 状态切换
export function GoodsStatus(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_status',
    method: 'post',
    data
  })
}


// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_dtl',
    method: 'get',
    params: { 'id': id }
  })
}


//删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_del',
    method: 'post',
    data
  })
}


// 分类
export function getTypes() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/card_types',
    method: 'get',
  })
}

// 
export function goodsSelect() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_select',
    method: 'get',
  })
}


// 商品 库存
export function goodsSelectInventory(gid) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_select_inventory',
    method: 'get',
    params: { 'gid': gid }
  })
}


export function goods_order_list(query) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_order_audit_list',
    method: 'get',
    params: {
      username: query.username,
      start_date: query.start_date,
      end_date: query.end_date,
      tid:query.tid,
      page: query.page,
      page_size: query.page_size
    }
  })
}

export function goods_order_audit(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_order_audit',
    method: 'post',
    data
  })
}

export function goods_order_grant_list(query) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_order_grant_list',
    method: 'get',
    params: {
      username: query.username,
      start_date: query.start_date,
      end_date: query.end_date,
      tid:query.tid,
      page: query.page,
      page_size: query.page_size
    }
  })
}


export function goods_order_grant(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/goods_order_grant',
    method: 'post',
    data
  })
}

export const fileSize = 20
