import request from '@/utils/request'
// 搜索
export function handleData() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/store_handle',
    method: 'post',
  })
}
export const store_exp = process.env.VUE_APP_BASE_API + '/lfmini/store-u-l'
