import request from '@/utils/request'

export function list(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/bot_tag_list',
    method: 'get',
    params: data
  })
}

// 搜索
export function search(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/bot_tag_list',
    method: 'post',
    data
  })
}

// 修改或新建
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/bot_tag_edit',
    method: 'post',
    data
  })
}

// 删除
export function deleted(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/bot_tag_del',
    method: 'post',
    data
  })
}

// 详情
export function detail(id) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/bot_tag_dtl',
    method: 'get',
    params: { 'id': id }
  })
}

export const fileSize = 20
