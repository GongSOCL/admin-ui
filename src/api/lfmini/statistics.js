import request from '@/utils/request'

// 注册统计
export function user(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/statistics/user',
    method: 'post',
    data
  })
}

// 注册统计导出
export function exportUser(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/statistics/exportUser',
    method: 'post',
    data
  })
}

// 文章统计
export function resource(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/statistics/resource',
    method: 'post',
    data
  })
}

// 文章统计导出
export function exportResource(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/statistics/exportResource',
    method: 'post',
    data
  })
}

// 问卷统计
export function question(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/statistics/question',
    method: 'post',
    data
  })
}

// 问卷统计导出
export function exportQuestion(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/lfmini/statistics/exportQuestion',
    method: 'post',
    data
  })
}
