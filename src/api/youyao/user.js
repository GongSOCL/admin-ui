import request from '@/utils/request'

//用户列表
export function userList(
  keywords = "",
  verify_status = 0,
  submitted_type = 0,
  real_type = 0,
  current = 1,
  limit = 10,
  verifyStatus = -1
) {
  return request({
    url: '/youyao/users',
    method: 'get',
    params: {
        keywords,
        current,
        limit,
        verify_status,
        submitted_type,
        real_type
    }
  })
}

//导出
export function exportUserList(
  keywords = "",
  verify_status = 0,
  submitted_type = 0,
  real_type = 0,
) {
  return request({
    url: '/youyao/users/exportUsers',
    method: 'post',
    params: {
        keywords,
        verify_status,
        submitted_type,
        real_type
    }
  })
}

//用户认证
export function userVerify(uid, is_pass = 1, real_roal, reason = "") {
    return request({
      url: '/youyao/users/verify',
      method: 'post',
      data: {
          uid,
          is_pass,
          real_roal,
          reason
      }
    })
}

//用户认证
export function getVerifyInfo(uid) {
  return request({
    url: '/youyao/users/verify',
    method: 'get',
    params: {
        uid
    }
  })
}

//服务列表
export function serveList(uid = 0, hospital_id = 0, series_id = 0, start ="", end = "", current = 1, limit =10) {
    return request({
      url: '/youyao/serve',
      method: 'get',
      params: {
          uid,
          hospital_id,
          series_id,
          start,
          end,
          current,
          limit
      }
    })
}

//服务审核
export function serveAudit(serve_id, is_pass = 0) {
    return request({
      url: '/youyao/serve/audit',
      method: 'post',
      data: {
         serve_id,
         is_pass
      }
    })
}

//服务审核
export function serveUnbindAudit(serve_id, is_pass = 0) {
  return request({
    url: '/youyao/serve/audit-unbind',
    method: 'post',
    data: {
       serve_id,
       is_pass
    }
  })
}

//用户认证
export function cancelVerify(uid) {
  return request({
    url: `/youyao/users/verify/${uid}`,
    method: 'delete',
  })
}

//用户搜索
export function search(keywords = "", limit = 10) {
  return request({
    url: '/youyao/users/search',
    method: 'get',
    params: {
        keywords,
        limit
    }
  })
}

//用户认证
export function types() {
  return request({
    url: `/youyao/users/types`,
    method: 'get'
  })
}

export function rebindUserCompany(user_id, company_id) {
  return request({
    url: '/youyao/users/rebind-company',
    method: 'post',
    data: {
      user_id,
      company_id
    }
  })
}
