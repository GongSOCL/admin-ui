import request from '@/utils/request'

// 搜索公司
export function searchCompany(keywords = '', limit = 5) {
  return request({
    url: "/youyao/company",
    method: "GET",
    params: {
      keywords,
      limit
    }
  })
}
