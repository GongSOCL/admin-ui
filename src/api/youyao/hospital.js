import request from '@/utils/request'

// 药品选项
export function getDrugSeries() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/youyao/serve/drug-series',
    method: 'get'
  })
}

// 医院列表
export function searchHospital(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/youyao/serve/hospital-list',
    method: 'post',
    data
  })
}

// 医院列表导出
export function exportHospital(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/youyao/serve/export-hospital-list',
    method: 'post',
    data
  })
}
