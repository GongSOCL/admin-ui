import request from '@/utils/request'

export function editReply(id, content) {
    return request({
        url: `/consult/reply/${id}`,
        method: "PUT",
        data: {
            content
        }
    })
}

//删除回复
export function delReply(id) {
    return request({
        url: `/consult/reply/${id}`,
        method: "DELETE"
    })
}