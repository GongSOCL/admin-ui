import  request from '@/utils/request'

//列表
export function listConsults(user_id = 0, keywords = "", total = -1, sort_total = 0, sort_create = -1, sort_reply = -1, current = 1, limit = 10) {
    return request({
        url: "/consults",
        params: {
            user_id,
            keywords,
            total,
            sort_total,
            sort_create,
            sort_reply,
            current,
            limit
        }
    })
}

//置顶
export function stickyConsult(id) {
    return request({
        url: `/consults/${id}/sticky`,
        method: "PUT"
    })
}

//取消置顶
export function cancelSticky(id) {
    return request({
        url: `/consults/${id}/sticky-cancel`,
        method: "PUT"
    })
}

//详情
export function detail(id) {
    return request({
        url: `/consults/${id}`,
        method: "GET"
    })
}

//回复
export function reply(id, content) {
    return request({
        url: `/consults/${id}/reply`,
        method: "POST",
        data: {
            content
        }
    })
}

//回复列表
export function listReplies(id, current, limit) {
    return request({
        url: `/consults/${id}/replies`,
        method: "GET",
        params: {
            current,
            limit
        }
    })
}

//导出
export function exportConsult(user_id = 0, keywords = "", total = -1) {
    return request({
        url: "/consults/export",
        method: "PUT",
        data: {
            user_id,
            keywords,
            total
        }
    })
}

export function delConsult(id) {
    return request({
        url: `/consults/${id}`,
        method: "DELETE"
    })
}