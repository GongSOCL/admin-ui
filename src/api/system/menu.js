import request from '@/utils/request'

// 查询菜单列表
export function listMenu(query) {
  return request({
    url: '/system/menus',
    method: 'get',
    params: query
  })
}

export function yyApplistMenu(query) {
  return request({
    url: '/organization/yyappmenu',
    method: 'get',
    params: query
  })
}

export function yyAppMenuDetail(id) {
  return request({
    url: '/organization/yyappmenu/detail?id='+id,
    method: 'get',
  })
}


export function addYYappMenu(data) {
  return request({
    url: '/organization/yyappmenu/add',
    method: 'post',
    data: data
  })
}

export function updateYYappMenu(id, data) {
  return request({
    url: `/organization/yyappmenu/edit`,
    method: 'post',
    data: data
  })
}

export function delYYappMenu(id, data) {
  return request({
    url: '/organization/yyappmenu/delete?id='+id,
    method: 'post',
    data: data
  })
}



// 查询菜单详细
export function getMenu(id) {
  return request({
    url: `/system/menu/${id}`,
    method: 'get'
  })
}

// 新增菜单
export function addMenu(data) {
  return request({
    url: '/system/menu',
    method: 'post',
    data: data
  })
}

// 修改菜单
export function updateMenu(id, data) {
  return request({
    url: `/system/menu/${id}`,
    method: 'put',
    data: data
  })
}

// 删除菜单
export function delMenu(id) {
  return request({
    url: `/system/menu/${id}`,
    method: 'delete'
  })
}
