import request from '@/utils/request'

//打卡原因列表
export function getOutReason() {
  return request({
    url: '/visit/getOutReason',
    method: 'get'
  })
}

//打卡定位清单搜索
export function search(data) {
  return request({
    url: '/visit/search',
    method: 'post',
    data
  })
}

//打卡状态修改
export function updateVisitEffState(data) {
  return request({
    url: '/visit/updateVisitEffState',
    method: 'post',
    data
  })
}

//打卡状态修改
export function getVisitDistinct(id) {
  return request({
    url: `/visit/${id}/distinct`,
    method: 'get'
  })
}

export function asyncExport(data) {
  return request({
    url: `/visit/export-async`,
    method: 'post',
    data: data
  })
}

export function separateExport(data) {
  return request({
    url: `/visit/export-separate`,
    method: 'post',
    data: data
  })
}

//打卡定位清单导出
export const visitExport = process.env.VUE_APP_BASE_API + '/visit/export'

//上传打卡状态修改
export const uploadChangeVisitEffState = process.env.VUE_APP_BASE_API + '/visit/uploadChangeVisitEffState'
