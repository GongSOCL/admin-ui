import request from '@/utils/request'

// 代表医生
export function doctor(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/doctor',
    method: 'post',
    data
  })
}

// 代表医生
export function orgDoctor(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/doctor-with-depart',
    method: 'post',
    data
  })
}

// 代表医生导出
export function exportDoctor(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/exportDoctor',
    method: 'post',
    data
  })
}

// 底层医生库
export function allDoctor(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/all-doctors',
    method: 'post',
    data
  })
}

// 底层医生库导出
export function exportAllDoctor(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/export-all-doctor',
    method: 'post',
    data
  })
}
