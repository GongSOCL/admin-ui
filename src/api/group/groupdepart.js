import request from '@/utils/request'

// 所有部门
export function departs() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/depart/departs',
    method: 'get'
  })
}

// 所有产品
export function products() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/depart/products',
    method: 'get'
  })
}

// 部门列表
export function list() {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/depart/list',
    method: 'get'
  })
}

// 部门新增编辑
export function update(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/depart/update',
    method: 'post',
    data
  })
}

// 部门删除
export function del(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/depart/del',
    method: 'post',
    data
  })
}

// 部门删除
export function detail(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/depart/detail',
    method: 'post',
    data
  })
}
