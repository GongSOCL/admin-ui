import request from '@/utils/request'

export function agentNumberOfDoctors(data) {
  console.log(data)
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/agent-num-of-doctors',
    method: 'get',
    params: data
  })
}

export function agentListOfDoctors(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/agent-list-of-doctors',
    method: 'get',
    params: data
  })
}

export function agentOfDoctorsDel(nids) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/agent-of-doctors-del',
    method: 'post',
    data: { 'nids': nids }
  })
}

export function agentNumOfDoctorsExp(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/agent-num-of-doctors-exp',
    method: 'post',
    data
  })
}

export function agentNumOfDoctorsDetailExp(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/agent-of-doctors-dtl-exp',
    method: 'post',
    data
  })
}

export function agentOfDoctorsExp(data) {
  return request({
    baseURL: process.env.VUE_APP_BASE_API,
    url: '/group/agent-of-doctors-exp',
    method: 'post',
    data
  })
}
