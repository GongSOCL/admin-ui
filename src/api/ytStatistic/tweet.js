import request from '@/utils/request'

export function tweet_list(year, month, page, page_size, s, column) {
     return request({
          url: `/youtie/tw_list`,
          method: 'get',
          params: {
            year,
            month,
            page: page,
            page_size: page_size,
            sort: s,
            column: column
          }
        })
}

//代表导出
export const tw_exp = process.env.VUE_APP_BASE_API + '/youtie/tw_exp'
