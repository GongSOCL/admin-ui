import { listAreas, listProvinces, listCities } from '@/api/pharmacy/area'
import { listMainStoresLimite, listBranchStoresLimit } from '@/api/pharmacy/store'

const getters = {
  getAreaId(state) {
    return state.area_id
  },
  getProvinceId(state) {
    return state.province_id
  },
  getCityId(state) {
    return state.city_id
  },
  getMainStoreId(state) {
    return state.main_store_id
  },
  getBranchStoreId(state) {
    return state.branch_store_id
  },
  getAreas(state) {
    return state.areas
  },
  getProvinces(state) {
    return state.provinces
  },
  getCities(state) {
    return state.cities
  },
  getMainStores(state) {
    return state.main_stores
  },
  getBranchStores(state) {
    return state.branch_stores
  }
}

const state = {
  areas: [{ id: 0, name: '全部' }],
  area_id: 0,
  provinces: [{ id: 0, name: '全部' }],
  province_id: 0,
  cities: [{ id: 0, name: '全部' }],
  city_id: 0,
  main_stores: [{ id: 0, name: '全部' }],
  main_store_id: 0,
  branch_stores: [{ id: 0, name: '全部' }],
  branch_store_id: 0
}

const mutations = {
  SET_AREAS(state, areas) {
    const item = areas.find(elem => elem.id == 0)
    if (item === undefined) {
      areas.unshift({ id: 0, name: '全部' })
    }
    state.areas = areas
  },
  SET_AREA_ID(state, aread_id) {
    state.area_id = aread_id
  },
  SET_PROVINCES(state, provinces) {
    let item = provinces.find(elem => elem.id == 0)
    if (item === undefined) {
      provinces.unshift({ id: 0, name: '全部' })
    }
    state.provinces = provinces
  },
  SET_PROVINCE_ID(state, province_id) {
    state.province_id = province_id
  },
  SET_CITIES(state, cities) {
    let item = cities.find(elem => elem.id == 0)
    if (item === undefined) {
      cities.unshift({ id: 0, name: '全部' })
    }
    state.cities = cities
  },
  SET_CITY_ID(state, city_id) {
    state.city_id = city_id
  },
  SET_MAIN_STORES(state, main_stores) {
    let item = main_stores.find(elem => elem.id == 0)
    if (item === undefined) {
      main_stores.unshift({ id: 0, name: '全部' })
    }
    state.main_stores = main_stores
  },
  SET_MAIN_STORE_ID(state, main_store_id) {
    state.main_store_id = main_store_id
  },
  SET_BRANCH_STORES(state, branch_stores) {
    let item = branch_stores.find(elem => elem.id == 0)
    if (item === undefined) {
      branch_stores.unshift({ id: 0, name: '全部' })
    }
    state.branch_stores = branch_stores
  },
  SET_BRANCH_STORE_ID(state, branch_store_id) {
    state.branch_store_id = branch_store_id
  }
}

const actions = {
  initial({ commit }) {
    // 获取所有大区及省份
    listAreas('')
      .then(resp => {
        commit('SET_AREAS', resp.data.list)
      })
  },
  changeArea({ commit, dispatch }, { area_id }) {
    commit('SET_AREA_ID', area_id)
    // 获取大区省份， 并设置省份列表
    listProvinces(area_id, '', 5)
      .then(resp => {
        commit('SET_PROVINCES', resp.data.list)
        commit('SET_PROVINCE_ID', 0)
        dispatch('changeProvince', { province_id: 0 })
      })
  },
  searchArea({ commit, state }, { keywords }) {
    const item = state.area_id > 0 ? state.areas.find(elem => elem.id == state.area_id) : undefined
    listAreas(keywords)
      .then(resp => {
        let data = resp.data.list
        if (item !== undefined) {
          data.unshift(item)
        }
        commit('SET_AREAS', data)
      })
  },
  changeProvince({ commit, dispatch }, { province_id }) {
    commit('SET_PROVINCE_ID', province_id)
    listCities(province_id, '', 5)
      .then(resp => {
        commit('SET_CITIES', resp.data.list)
        commit('SET_CITY_ID', 0)
        dispatch('changeCity', { city_id: 0 })
      })
  },
  searchProvince({ commit, state }, { keywords }) {
    const item = state.city_id > 0 ? state.provinces.find(elem => elem.id == state.city_id) : undefined
    listProvinces(state.area_id, keywords, 5)
      .then(resp => {
        let data = resp.data.list
        if (item !== undefined) {
          data.unshift(item)
        }
        commit('SET_PROVINCES', data)
      })
  },
  changeCity({ commit, dispatch }, { city_id }) {
    commit('SET_CITY_ID', city_id)
    listMainStoresLimite(city_id, '', 5)
      .then(resp => {
        commit('SET_MAIN_STORES', resp.data.list)
        dispatch('changeMainStore', 0)
      })
  },
  searchCity({ commit, state }, { keywords }) {
    const item = state.city_id > 0 ? state.cities.find(elem => elem.id == state.city_id) : undefined
    listCities(state.province_id, keywords, 5)
      .then(resp => {
        let data = resp.data.list
        if (item !== undefined) {
          data.unshift(item)
        }
        commit('SET_CITIES', resp.data.list)
      })
  },
  changeMainStore({ commit, dispatch, state }, { main_store_id }) {
    commit('SET_MAIN_STORE_ID', main_store_id)
    listBranchStoresLimit(
      state.area_id,
      state.province_id,
      state.city_id,
      main_store_id,
      '',
      5
    ).then(resp => {
      commit('SET_BRANCH_STORES', resp.data.list)
      dispatch('changeBranchStore', 0)
    })
  },
  searchMainStore({ commit, state }, { keywords }) {
    const item = state.main_store_id > 0 ? state.main_stores.find(elem => elem.id == state.main_store_id) : undefined
    listMainStoresLimite(state.city_id, keywords, 5)
      .then(resp => {
        let data = resp.data.list
        if (item !== undefined) {
          data.unshift(item)
        }
        commit('SET_MAIN_STORES', data)
      })
  },
  changeBranchStore({ commit }, { branch_store_id }) {
    commit('SET_BRANCH_STORE_ID', branch_store_id)
  },
  searchBranchStore({ commit, state }, { keywords }) {
    const bsId = state.branch_store_id
    const item = bsId > 0 ? state.branch_stores.find(elem => elem.id == bsId) : undefined
    listBranchStoresLimit(
      state.area_id,
      state.province_id,
      state.city_id,
      state.main_store_id,
      keywords,
      5
    ).then(resp => {
      let data = resp.data.list
      if (item !== undefined) {
        data.unshift(item)
      }
      commit('SET_BRANCH_STORES', data)
    })
  }
}

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions
}
