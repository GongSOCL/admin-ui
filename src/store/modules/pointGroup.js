import { getDefaultGroups, searchGroups, infoGroup } from '@/api/point/group'
import Vue from 'vue'
    
const getters = {
    getList: (state) => {
        return state.list
    },
    getId(state) {
        return state.id
    },
    getDefaultId(state) {
        return state.id['common']
    }
}

const state = {
    platform: {
        common: 8,
        abc: 9
    },
    list: {
        common: []
    },
    select: {
        common: 0
    },
    id: {
        common: 0
    }
}

const mutations = {
    SET_PLATFORM: (state, {platform, mark}) => {
        if (mark == '' || mark == undefined) {
            mark = 'common'
        }
        Vue.set(state.platform, mark, platform)
    },
    SET_LIST: (state, {list, mark}) => {
        if (mark == '' || mark == undefined) {
            mark = 'common'
        }
        Vue.set(state.list, mark, list)
    },
    SET_SELECT: (state, {select, mark}) => {
        if (mark == '' || mark == undefined) {
            mark = 'common'
        }
        Vue.set(state.select, mark, select)
    },
    SET_ID: (state, {id, mark}) => {
        if (mark == '' || mark == undefined) {
            mark = 'common'
        }
        if (id != undefined) {
            Vue.set(state.id, mark, id)
        }
        console.log(11122233, mark, id, state.id)
    }
}

const actions = {
    initialize({ commit }, { platform, select, mark }) {
        console.log(platform, select, mark)
        if (select > 0) {
            commit('SET_PLATFORM', {platform, mark})
            commit('SET_SELECT', {select, mark})
            infoGroup(select)
                .then(resp => {
                    commit("SET_LIST", {
                        list: [resp.data.info], 
                        mark: mark
                    })
                    commit('SET_ID', {
                        id: resp.data.info.id, 
                        mark: mark
                    })
                })
        } else {
            commit('SET_PLATFORM', {platform, mark})
            console.log(platform)
            getDefaultGroups(platform)
                .then(resp => {
                    commit("SET_LIST", {
                        list: [resp.data.info], 
                        mark: mark
                    })
                    commit('SET_ID', {
                        id: resp.data.info.id, 
                        mark: mark
                    })
                })
        }
    },
    changePlatform({ commit }, {platform, mark}) {
        commit('SET_PLATFORM', {platform, mark})
        getDefaultGroups(platform)
            .then(resp => {
                commit("SET_LIST", {
                    list: [resp.data.info], 
                    mark: mark
                })
                commit('SET_ID', {
                    id: resp.data.info.id, 
                    mark: mark
                })
            })
    },
    searchGroups({ commit, state }, {cb, mark}) {
        searchGroups(state.platform[mark], cb, 5)
            .then(resp => {
                commit('SET_LIST', {
                    list: resp.data.list, 
                    mark: mark
                })
            })
    }
}

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}