import { listPaper, deletePaper, publishPaper, offShelfPaper, addPaper } from '@/api/questionnaire/questionnaire'

// eslint-disable-next-line no-unused-vars
import vuex from 'vuex'

const state = {
  keywords: '',
  status: 0,
  current: 1,
  papers: [],
  pages: {
    'current': 1,
    'pages': 0,
    'size': 10,
    'total': 0
  },
  processing: false,
  publishSuccess: false,
  addSuccess: false,
  deleteSuccess: false,
  offShelfSuccess: false
}

const getters = {
  getPapers(state) {
    return state.papers.map(item => {
      const typeGet = function(type) {
        // eslint-disable-next-line eqeqeq
        return type == 0 ? '必答问卷' : (type == 1 ? '信息收集问卷' : '-')
      }
      const statusGet = function(status) {
        status = parseInt(status)
        switch (status) {
          case 1:
            return '未发布'
          case 2:
            return '已发布'
          case 3:
            return '已删除'
          case 4:
            return '已过期'
        }
      }
      const platformGet = function(platform) {
        platform = parseInt(platform)
        switch (platform) {
          case 1:
            return '优佳医'
          case 2:
            return 'OTC'

          case 3:
            return '药店关联小程序'
          default:
            return '优药'
            
        }
      }
      return {
        id: item.id,
        name: item.name,
        type: typeGet(item.type),
        total: item.total,
        publishAt: item.publish_at,
        expireAt: item.expire_at,
        status: statusGet(item.status),
        // eslint-disable-next-line eqeqeq
        enablePublish: item.status == 1,
        // eslint-disable-next-line eqeqeq
        enableOffshelf: item.status == 2,
        // eslint-disable-next-line eqeqeq
        enableEdit: item.status == 1,
        // eslint-disable-next-line eqeqeq
        enableQuestionPaperEdit: item.type == 0 && item.status != 3,
        // eslint-disable-next-line eqeqeq
        enableQuestionEdit: item.type == 1 && item.status != 3,
        // eslint-disable-next-line eqeqeq
        enableDelete: item.status != 3,
        platform: platformGet(item.platform),
        // eslint-disable-next-line eqeqeq
        enableShare: item.is_invite == 0 && item.status == 2,

        // eslint-disable-next-line eqeqeq
        enableDown: item.is_invite == 0 && item.status == 2 && item.show_type == 2
      }
    })
  },
  getPages(state) {
    return state.pages
  },
  getIsProcessing(state) {
    return state.processing
  },
  getTotal(state) {
    return state.pages.total
  },
  getCurrentPage(state) {
    return state.pages.current
  }
}

const actions = {
  // 初始化
  initPapers({ commit, dispatch }) {
    commit('hideProcessing')
    const commitData = { keywords: '', status: 0 }
    commit('setKeywordAndStatus', commitData)
    dispatch('changeCond', '', 0)
  },
  // 修改搜索条件
  async changeCond({ commit, dispatch }, { keywords, status }) {
    commit('updateProcessing')
    const commitData = { keywords: keywords, status: status }
    commit('setKeywordAndStatus', commitData)
    commit('setCurrentPage', 1)
    await dispatch('listPage')
    commit('updateProcessing')
  },
  // 问卷列表
  async listPage({ commit, state }) {
    listPaper(state.status, state.keywords, state.current)
      .then(response => {
        commit('setPapers', response)
      })
  },
  // 分页跳转
  async changePage({ commit, dispatch }, page) {
    commit('updateProcessing')
    commit('setCurrentPage', page)
    await dispatch('listPage')
    commit('updateProcessing')
  },
  async publish({ commit, state }, id) {
    commit('reverseSuccess', 'publishSuccess')
    commit('updateProcessing')
    const idx = state.papers.findIndex(function(item) {
      return item.id == id
    })
    if (idx == -1) {
      return false
    }
    const item = {
      index: idx,
      status: 2,
      publishTime: ''
    }
    publishPaper(id)
      .then(response => {
        commit('updatePaperItemStatus', item)
        commit('setSuccess', 'publishSuccess')
      }).catch(err => {
        // console.log(err)
      })
    commit('updateProcessing')
  },
  async offShelf({ commit, state }, id) {
    commit('reverseSuccess', 'offShelfSuccess')
    commit('updateProcessing')
    const idx = state.papers.findIndex(function(item) {
      return item.id == id
    })
    if (idx == -1) {
      return false
    }
    const item = {
      index: idx,
      status: 1,
      publishTime: ''
    }
    offShelfPaper(id)
      .then(response => {
        commit('updatePaperItemStatus', item)
        commit('setSuccess', 'offShelfSuccess')
      }).catch(err => {
      })
    commit('updateProcessing')
  },
  async addOnePaper({ commit, dispatch }, { name, type, platform, autoPublish, showType }) {
    commit('reverseSuccess', 'addSuccess')
    commit('updateProcessing')
    commit('setCurrentPage', 1)
    addPaper(name, type, platform, autoPublish ? 1 : 0, showType).then(response => {
      commit('setSuccess', 'addSuccess')
      dispatch('listPage')
      // eslint-disable-next-line handle-callback-err
    }).catch(err => {})
    commit('updateProcessing')
  },
  async deleteOnePaper({ commit, state }, id) {
    commit('reverseSuccess', 'deleteSuccess')
    commit('updateProcessing')
    const idx = state.papers.findIndex(function(item) {
      return item.id == id
    })
    if (idx == -1) {
      return false
    }
    const dataItem = {
      index: idx
    }
    deletePaper(id)
      .then(response => {
        commit('deletePaper', dataItem)
        commit('setSuccess', 'deleteSuccess')
      }).catch(error => {})
    commit('updateProcessing')
  }
}

const mutations = {
  setKeywordAndStatus(state, { keywords, status }) {
    state.keywords = keywords
    state.status = status
  },
  setCurrentPage(state, current) {
    state.current = current
  },
  setPapers(state, response) {
    state.papers = response.data.papers
    state.pages = response.data.pages
  },
  updatePaperItemStatus(state, { index, status, publishTime }) {
    const itemData = JSON.parse(JSON.stringify(state.papers[index]))
    itemData.status = status
    itemData.publish_time = publishTime

    state.papers.splice(index, 1, itemData)
  },
  deletePaper(state, { index }) {
    const itemData = JSON.parse(JSON.stringify(state.papers[index]))
    itemData.status = 3
    state.papers.splice(index, 1, itemData)
  },
  updateProcessing(state) {
    state.processing = !state.processing
  },
  hideProcessing(state) {
    state.processing = false
  },
  setSuccess(state, item) {
    const opt = [
      'publishSuccess',
      'addSuccess',
      'deleteSuccess',
      'offShelfSuccess'
    ]
    if (!opt.includes(item)) {
      return
    }
    state[item] = true
  },
  reverseSuccess(state, item) {
    const opt = [
      'publishSuccess',
      'addSuccess',
      'deleteSuccess',
      'offShelfSuccess'
    ]
    if (!opt.includes(item)) {
      return
    }
    state[item] = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
