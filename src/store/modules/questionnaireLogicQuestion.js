import { infoPaper } from '@/api/questionnaire/questionnaire'
import { addLogicQuestions, listLogicQuestions, deleteLogicQuestions, editLogicQuestions, sortLogicQuestions, getValidNextQuestion } from '@/api/questionnaire/logic_question'
import { addOptions, deleteOptions, editOptions } from '@/api/questionnaire/options'
import axios from 'axios'

const state = {
     paper: {
          id: 0,
          status: 1,
          name: '',
          welcome: '',
          total_quota: 0,
          expire_at: '',
          cash: 0,
          points: 0
     },
     questions: [],
     logics: [],
     editLogics: [],
     editLogicNextQuestions: [],
     editQuestion: {}
};

const getters = {
     getQuestions(state) {
          const typeGetter = function(type) {
               switch (type) {
                    case 1:
                      return '单选题'
                      break
                    case 2:
                      return '多选题'
                      break
                    case 3:
                      return '填空题'
                      break
                    default:
                      return '-'
                      break
                  }
          };
          return state.questions.map(function(item){
               return {
                    id: item.id,
                    name: item.name,
                    type: typeGetter(item.type),
                    enableEdit: state.paper.status == 1,
                    enableDelete: state.paper.status == 1,
                    origin: item
               }
          });
     },
     getPaperStatus(state) {
          return state.paper.status
     },
     getLogics(state) {
          //获取编辑题目的所有逻辑规则
          return state.editLogics
     },
     getNextQuestions(state) {
          //获取编辑题目的下一题目
          return state.editLogicNextQuestions
     }
};

const actions = {
     initPaperLogic({ commit }, id) {
          return axios.all([infoPaper(id), listLogicQuestions(id)])
          .then(axios.spread(function(infoResp, listQuestionResp) {
               commit('setPapers', infoResp)
               commit('setQuestions', listQuestionResp)
          }))
     },
     getQuestionLogicsAndNextQuestions({ commit }, questionId) {

          commit('setEditLogics', ),
          commit('editLogicNextQuestions')
     },
     getNextQuestions({ commit }, questionId) {

     },
     getLogicEditQuestion({ commit }, questionId) {
          commit('setEditQuestion', questionId)
     },
     async editLogicQuestion({ commit }, edq) {
          return editLogicQuestions(
               edq.id,
               edq.name,
               edq.type,
               edq.options,
               edq.is_skip
          ).then(response => {
               commit("setEditQuestionName", edq)
          })
     },
     async deleteLogicQuestionOption({commit }, {questionID, optionId}) {
           //TODO: emit + vuex
          return deleteOptions(optionId).then(response => {
               commit("removeQuestionOption", {questionID, optionId})
          })
     },
     async editQuestionOption({ commit }, {questionID, id, name, isRight}) {
          return editOptions(
               id,
               name,
               isRight
          ).then(response => {
               commit('editQuestionOption', {questionID, id, name, isRight})
          })
     },
     async addOptions({ commit }, {questionID, name, isRight}) {
          return addOptions(
               questionID,
               name,
               isRight
          ).then(response => {
               commit('addQuestionOption', {
                    questionID, 
                    name, 
                    isRight,
                    id: response.data.id
               })
          })    
     },
     async refreshPaperQuestions({commit, state}) {
          return listLogicQuestions(state.paper.id)
               .then(response => {
                    commit('setQuestions', response) 
               });
     },
     async deleteQuestion({ commit }, questionID) {
          return deleteLogicQuestions(questionID).then(response => {
               commit("deleteQuestion", questionID)
             })
     },
     async addOneLogicQuestion({commit, state, dispatch}, {name, type, options, isSkip}) {
          return addLogicQuestions(
               state.paper.id,
               name,
               type,
               options,
               isSkip
             ).then(response => {
               if (options.length > 0) {
                    dispatch('refreshPaperQuestions')
               } else {
                    commit('addNewQuestionItem', {
                         id: response.data.id,
                         name: name,
                         type: type,
                         options: options,
                         isSkip: isSkip
                    })
               }
             })
     }  
};

const mutations = {
     setPapers(state, papers) {
          state.paper = papers.data
     },
     setQuestions(state, response) {
          state.questions = response.data.questions
     },
     setEditLogics(state, response) {
          state.editLogics = response.data
     },
     editLogicNextQuestions(state, response) {
          state.editLogicNextQuestions = response.data
     },
     setEditQuestionName(state, edq) {
          const idx = state.questions.findIndex(function(item) {
               return edq.id == item.id
          })
          if (idx != -1) {
               let questionItem = JSON.parse(JSON.stringify(state.questions[idx]))
               questionItem.name = edq.name
               questionItem.is_skip = edq.is_skip

              state.questions.splice(idx, 1, questionItem)
          }
     },
     removeQuestionOption(state, {questionID, optionId}) {
          const idx = state.questions.findIndex(function(item) {
               return questionID == item.id
          })
          if (idx != -1) {
               let questionItem = JSON.parse(JSON.stringify(state.questions[idx]))
               const optIdx = questionItem.options.findIndex(function(item) {
                    return optionId == item.id;
               });
               if (optIdx != -1) {
                    questionItem.options.splice(optIdx, 1);
                    state.questions.splice(idx, 1, questionItem)
               }
          }
     },
     editQuestionOption(state, {questionID, id, name, isRight}) {
          const idx = state.questions.findIndex(function(item) {
               return questionID == item.id
          })
          if (idx != -1) {
               let questionItem = JSON.parse(JSON.stringify(state.questions[idx]))
               console.log(questionItem)
               const optIdx = questionItem.options.findIndex(function(item) {
                    return id == item.id;
               });
               if (optIdx != -1) {
                    questionItem.options[optIdx].id = id;
                    questionItem.options[optIdx].name = name;
                    questionItem.options[optIdx].is_right = isRight;
                    
                    if (isRight && questionItem.type == 1) {
                         const opts = questionItem.options.map(function(item) {
                              if (item.id != id) {
                                   item.is_right = false;
                              }
                              return item;
                         });
                    }
                    state.questions.splice(idx, 1, questionItem)
               }
          }
     },
     addQuestionOption(state, {questionID, id, name, isRight}) {
          const idx = state.questions.findIndex(function(item) {
               return questionID == item.id
          })
          if (idx != -1) {
               const optionItem = {
                    id: id,
                    name: name,
                    is_right: isRight
               }
               let questionItem = JSON.parse(JSON.stringify(state.questions[idx]))
               if (isRight && questionItem.type == 1) {
                    const opts = questionItem.options.map(function(item) {
                         if (item.id != id) {
                              item.is_right = false;
                         }
                         return item;
                    });
                    questionItem.options = opts;
               }
               questionItem.options.push(optionItem)
               state.questions.splice(idx, 1, questionItem)
          }
     },
     deleteQuestion(state, questionID) {
          const idx = state.questions.findIndex(item => {
               return item.id == questionID
          })
          if (idx != -1) {
               state.questions.splice(idx, 1)
          }
     },
     addNewQuestionItem: function(state, {id, name, type, options, isSkip}) {
          state.questions.push({
            id: id,
            name: name,
            type: type,
            options: options,
            is_skip: isSkip
          })
        },
};

export default {
     namespaced: true,
     state,
     getters,
     actions,
     mutations
}